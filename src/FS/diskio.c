/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2016        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "ff.h"			/* Obtains integer types */
#include "diskio.h"		/* Declarations of disk functions */
#include "sdcard.h"

/* Definitions of physical drive number for each drive */
#define DEV_RAM		0	/* Example: Map Ramdisk to physical drive 0 */
#define DEV_MMC		1	/* Example: Map MMC/SD card to physical drive 1 */
#define DEV_USB		2	/* Example: Map USB MSD to physical drive 2 */
#define SECTOR_SIZE 512

/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber to identify the drive */
)
{
	switch (pdrv)
	    {
	        case 0:
	        {
	            uint32_t status;
	            return SD_SendStatus(&status) == SD_OK ? RES_OK : RES_NOTRDY;
	        }
	    }

	    return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{

	return SD_OK;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	DWORD sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
{
	switch (pdrv)
	{
	  case 0:
	  {
		  if(count==1)
		  {
			  SD_Error status = SD_ReadBlock(sector << 9 ,(uint32_t*)buff,SECTOR_SIZE);
			  if (status != SD_OK)
				 return RES_ERROR;
		  }
		  else
		  {
			  SD_Error status = SD_ReadMultiBlocks(sector << 9, (uint32_t*)buff,SECTOR_SIZE,count);
			  if (status != SD_OK)
			  return RES_ERROR;
		  }
	  }
	}

	  return RES_OK;

}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	DWORD sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{
	switch (pdrv)
	{
	 case 0:
	 {
		  if(count==1)
		  {
			SD_Error status = SD_WriteBlock(sector << 9 ,(uint32_t*)buff, SECTOR_SIZE);
			if (status != SD_OK)
			  return RES_ERROR;
		  }
		  else
		  {
			SD_Error status = SD_WriteMultiBlocks(sector << 9 ,(uint32_t*)buff, SECTOR_SIZE, count);
			if (status != SD_OK)
			 return RES_ERROR;
		  }
	 }
	}

	  return RES_OK;
}

#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
	DRESULT res = RES_ERROR;
	SD_CardInfo SDCardInfo;

	  switch (cmd)
	  {
	    case CTRL_SYNC:
	      res = RES_OK;
	      break;

	    case GET_SECTOR_COUNT:
	    	SD_GetCardInfo(&SDCardInfo);
	      *(DWORD*)buff = SDCardInfo.CardCapacity / 512;
	      res = RES_OK;
	      break;

	    case GET_SECTOR_SIZE:
	      *(DWORD*)buff = 512;
	      res= RES_OK;
	      break;

	    case GET_BLOCK_SIZE:
	      *(DWORD*)buff = 512;
	      res= RES_OK;
	      break;
	  }

	  return res;
}
/*
DWORD get_fattime (void)
{
    return 0;
}
*/
/*******************************************************************************
* Function Name  : get_fattime
* Description    : Displays the current time.
* Input          : - TimeVar: RTC counter value.
* Output         : None
* Return         : None
*******************************************************************************/
#define SecsPerComYear  31536000//(365*3600*24)
#define SecsPerLeapYear 31622400//(366*3600*24)
#define SecsPerFourYear 126230400//((365*3600*24)*3+(366*3600*24))
#define SecsPerDay      86400   //(3600*24)

s32 Year_Secs_Accu[5] = {0,31622400,63158400,94694400,126230400};
s32 Month_Secs_Accu_C[13] = {0,2678400,5097600,7776000,10368000,13046400,15638400,18316800,20995200,23587200,26265600,28857600,31536000};
s32 Month_Secs_Accu_L[13] = {0,2678400,5184000,7862400,10454400,13132800,15724800,18403200,21081600,23673600,26352000,28944000,31622400};

DWORD get_fattime(void){
	DWORD CurrentTime = 0;
#if 1
    u32 RTCMount = 0;
    u32 TY = 0, TM = 1, TD = 1, THH = 0, TMM = 0, TSS = 0;
    s32 Num4Y = 0, NumY = 0, OffSec = 0, Off4Y = 0, NumDay = 0;//OffDay;
#endif
    u32 i = 0;
    RTCMount = RTC_GetCounter();
    Num4Y = RTCMount/SecsPerFourYear;//�ж��ٸ�������
    OffSec = RTCMount%SecsPerFourYear;//���������꣬ʣ�¶�����
    i = 1;
    while(OffSec > Year_Secs_Accu[i++])
        Off4Y++;//���������껹�м��꣬OffSec����1

    /* Numer of Complete Year */
    NumY = Num4Y*4 + Off4Y;
    TY = NumY << 25;//�õ���� bit31:25 Year from 1980 (0..127)

    OffSec = OffSec - Year_Secs_Accu[i-2];//����һ����·ݹ��ж����룬OffSec����2

    /* Month (TBD with OffSec)*/
    i = 0;
    if(Off4Y != 0)
    {// common year
        while(OffSec > Month_Secs_Accu_C[i++]);
        TM = i-1;
        OffSec = OffSec - Month_Secs_Accu_C[i-2];//����һ�µ��칲�ж����룬OffSec����3
    }
    else
    {// leap year
        while(OffSec > Month_Secs_Accu_L[i++]);
        TM = i-1;
        OffSec = OffSec - Month_Secs_Accu_L[i-2];
    }
    TM = TM << 21;//bit24:21 Month (1..12)

    /* Date (TBD with OffSec) */
    NumDay = OffSec/SecsPerDay;
    TD = (NumDay+1) << 16;//bit20:16 Day in month(1..31)

    OffSec = OffSec%SecsPerDay;//����һ���ʱ���ﻹʣ�����룬OffSec����34

    /* Compute  hours */
    THH = (OffSec/3600) << 11;//bit15:11 Hour (0..23)
    /* Compute minutes */
    TMM = ((OffSec % 3600)/60) << 5;//bit10:5 Minute (0..59)
    /* Compute seconds */
    TSS = ((OffSec % 3600)% 60) >> 1;//bit4:0 Second / 2 (0..29)

    CurrentTime = TY + TM + TD + THH + TMM + TSS;
    return CurrentTime;
}
