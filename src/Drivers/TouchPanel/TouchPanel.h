#ifndef __TOUCH_7846_H
#define __TOUCH_7846_H
#include "stdbool.h"
#include <stdio.h>
#ifdef __cplusplus
 extern "C" {
#endif

#include "stm32f10x_spi.h"
#include "stm32f10x_gpio.h"
#include <SPI.h>
#include <GPIO.h>
#include "GUI.h"
#include "Delay.h"
#include <EEPROM_map.h>
#include "EXTI.h"


#define TOUCH_PRESSED	1
#define TOUCH_UNPRESSED	0
#define LCD_PIXEL_WIDTH			480
#define LCD_PIXEL_HEIGHT		800

#define TOUCH_NON_LINEAR_X		10.5
#define TOUCH_NON_LINEAR_Y		5.8

#define SPI_PORT	SPI2

#define TOUCH_CS_PORT	 GPIOB
#define TOUCH_CS_PIN	 GPIO_Pin_12

#define TOUCH_IRQ_PORT	 GPIOB
#define TOUCH_IRQ_PIN	 GPIO_Pin_0

#define T_CS()   GPIO_ResetBits ( TOUCH_CS_PORT, TOUCH_CS_PIN );
#define T_DCS()  GPIO_SetBits ( TOUCH_CS_PORT, TOUCH_CS_PIN );


#define TOUCH_READ_CORRECTED		0x0F
#define TOUCH_READ_NOT_CORRRECTED	0xFF



typedef struct
{
	int16_t ax;
	int16_t bx;
	int16_t ay;
	int16_t by;
	uint8_t  isReadCorrected;
}touchCoefs_t;

typedef void (*pvWriteToEEPword)(uint16_t addr, uint16_t data);
typedef void (*pvWriteToEEPbyte)(uint16_t addr, uint8_t data);
typedef uint16_t (*pvReadFromEEPword)(uint16_t addr);
typedef uint8_t (*pvReadFromEEPbyte)(uint16_t addr);
// �������������
void TouchInit ( void );
void TouchCalibrate (void);

// ���������� ���������
void TouchReadStore ( void );
bool TouchVerifyCoef(void);
void WriteCoeffsToEEPROM(pvWriteToEEPword pvWriteWordToEEPROM,
		pvWriteToEEPbyte pvWriteByteToEEPROM);
void ReadCoeffsFromEEPROM(pvReadFromEEPword pvReadWordFromEEPROM,
		pvReadFromEEPbyte pvReadByteFromEEPROM	);

void TPReceiveX();
void TPGetXData();
bool TouchReadXY_DMA(uint16_t *px, uint16_t *py);
// ���� ������� ��� ���
bool isTouch ( void );

#ifdef __cplusplus
}
#endif

#endif
