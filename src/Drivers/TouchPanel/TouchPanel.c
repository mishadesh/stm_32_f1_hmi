#include "TouchPanel.h"

touchCoefs_t touchCoefs = {0xFFFF};

static uint16_t TPReadX ( void );
static uint16_t TPReadY ( void );
static bool TouchReadXY ( uint16_t *px, uint16_t *py );

static bool dataRDY = false;
static bool RecvY = false;

static const int16_t xCenter[] = { 35, LCD_PIXEL_WIDTH-35, 35, LCD_PIXEL_WIDTH-35 };/* standart calibr */
								//	, 35, LCD_PIXEL_WIDTH-35, LCD_PIXEL_WIDTH/2, LCD_PIXEL_WIDTH/2, LCD_PIXEL_WIDTH/2};

static const int16_t yCenter[] = { 35, 35, LCD_PIXEL_HEIGHT-35, LCD_PIXEL_HEIGHT-35 };/* standart calibr */
									//,LCD_PIXEL_HEIGHT/2, LCD_PIXEL_HEIGHT/2, 35, LCD_PIXEL_HEIGHT-35, LCD_PIXEL_HEIGHT/2};

uint16_t xRead[20], yRead[20];
uint8_t * responseX, responseY;
static int16_t xPos[4], yPos[4];


void WriteCoeffsToEEPROM(pvWriteToEEPword pvWriteWordToEEPROM,
		pvWriteToEEPbyte pvWriteByteToEEPROM)
{
	pvWriteWordToEEPROM(TOUCH_COEF_AX, touchCoefs.ax);
	pvWriteWordToEEPROM(TOUCH_COEF_BX, touchCoefs.bx);
	pvWriteWordToEEPROM(TOUCH_COEF_AY, touchCoefs.ay);
	pvWriteWordToEEPROM(TOUCH_COEF_BY, touchCoefs.by);
	pvWriteByteToEEPROM(TOUCH_IS_CALIBRATED, touchCoefs.isReadCorrected);
}

void ReadCoeffsFromEEPROM(pvReadFromEEPword pvReadWordFromEEPROM,
		pvReadFromEEPbyte pvReadByteFromEEPROM	)
{
	touchCoefs.ax = pvReadWordFromEEPROM(TOUCH_COEF_AX);
	touchCoefs.bx = pvReadWordFromEEPROM(TOUCH_COEF_BX);
	touchCoefs.ay = pvReadWordFromEEPROM(TOUCH_COEF_AY);
	touchCoefs.by = pvReadWordFromEEPROM(TOUCH_COEF_BY);
	touchCoefs.isReadCorrected = pvReadByteFromEEPROM(TOUCH_IS_CALIBRATED);
}

void TouchInit ( void )
{
	SPI_vInit_SPI2();
	SPI2_DMA_Enable();
	DMA1_SPI2_Init(SPI2_GetRxBufAddr(), SPI2_GetTxBufAddr(), 3);
	GPIO_Init_TouchPanel();
	EXTI_vRegisterPB0ActionCb(TPReceiveX);
	EXTI_vSetPB0_ActionEnable(true);
	EXTI_TOUCH_IRQ_Init();
}

static void TouchvInitCalibrate()
{
	SPI2_DMA_Disable();
	SPI_vInit_SPI2();
	GPIO_Init_TouchPanel();
	EXTI_vSetPB0_ActionEnable(false);
}

void TPReceiveY()
{
	uint8_t tx_cmd[3];
	tx_cmd[0] = 0xD2; tx_cmd[1] = 0x00; tx_cmd[2] = 0x00;
	SPI2_DMA_Send(tx_cmd, 3);
	RecvY = true;
}

void TPReceiveX()
{
	uint8_t tx_cmd[3];
	tx_cmd[0] = 0x92; tx_cmd[1] = 0x00; tx_cmd[2] = 0x00;
	SPI2_DMA_Send(tx_cmd, 3);
	RecvY = false;
}
void TPGetXData()
{
	uint8_t * rxdat = SPI2_GetRxBufAddr();
	static uint8_t count = 0;
	/*
	if (RecvZ && !RecvXZ)
	{
		if (count < 20)
		{
			zRead[count] = (rxdat[1]<<8 | rxdat[2])>>3;
			TPReceiveZ2();
			count++;
		}
		else
		{
			TPReceiveX();
			RecvXZ = true;
			count = 0;
		}
	} else if (RecvZ && RecvXZ)
	{
		if (count < 20)
		{
			xRead[count] = (rxdat[1]<<8 | rxdat[2])>>3;
			TPReceiveX();
			count++;
		}
		else
		{
			count = 0;
			RecvZ = RecvXZ = false;
			uint32_t z_avg = 0, x_avg = 0, z = 0;
			uint16_t x = 0, z2 = 0;
			for (uint8_t i = 0; i < 20; i++)
			{
				z_avg+= zRead[i]; x_avg+= xRead[i];
			}
			x = x_avg/20; z2 = z_avg/20;
			z = z2 + 4095; z -= x;
			if (z >= 5000)
			TPReceiveX();
			else
			EXTI_vSetPB0_ActionEnable(true);
		}
	}

	else
	*/
	{
		if (!RecvY)
		{
			if (count < 20)
			{
				xRead[count] = (rxdat[1]<<8 | rxdat[2])>>3;
				TPReceiveX();
				count++;
			}
			else
			{
				TPReceiveY();
				count = 0;
			}
		}
		else
		{
			if (count < 20)
			{
				yRead[count] = (rxdat[1]<<8 | rxdat[2])>>3;
				TPReceiveY();
				count++;
			}
			else
			{
				count = 0;
				dataRDY = true;
			}
		}
	}
}

static uint16_t TPReadX ( void )
{
	uint16_t x;
	T_CS ( );
	x = SPI_Touch_Write ( 0x92, SPI_PORT );
	T_DCS ( );
	return x;
}

static uint16_t TPReadY ( void )
{
	uint16_t y;
	T_CS ( );
	y = SPI_Touch_Write ( 0xD2, SPI_PORT );
	T_DCS ( );
	return y;
}

static bool TouchReadXY ( uint16_t *px, uint16_t *py )
{
	bool flag;
	int16_t x, y, xx, yy;
	uint32_t x_s = 0, y_s = 0;
	flag = isTouch();
	if ( flag )
	{
		//vDelayUsCycle(5);
		for (uint8_t i = 0; i < 20; i++)
		{
			x_s += TPReadX();
			vDelayUsCycle(1);
			y_s += TPReadY();

			vDelayUsCycle(1);
		}
			x=x_s/20;
			y=y_s/20;


		if ( TouchVerifyCoef() )
		{
			xx = ( x / touchCoefs.ax ) + touchCoefs.bx;
			xx = xx - (xx/TOUCH_NON_LINEAR_X);
			yy = ( y / touchCoefs.ay ) + touchCoefs.by;
			yy = (uint16_t)( yy - ((float)yy/TOUCH_NON_LINEAR_Y));
			if (xx > LCD_PIXEL_WIDTH)
				*px = LCD_PIXEL_WIDTH;
			else
				*px = xx;

			if (yy > LCD_PIXEL_HEIGHT)
				*py = LCD_PIXEL_HEIGHT;
			else
				*py = yy;
		}
		else
		{
			*px = x;
			*py = y;
		}
	}

	return flag;
}



bool TouchReadXY_DMA(uint16_t *px, uint16_t *py)
{
	static int16_t x = 0, y = 0, xx = 0, yy = 0;
	uint32_t x_s = 0, y_s = 0;
	if (dataRDY)
	{
		for (uint8_t i = 0; i < 20; i++)
		{
			x_s += xRead[i];
			y_s += yRead[i];
		}
		x=x_s/20;
		y=y_s/20;

		//x = xRead[0];
		//y = yRead[0];
		if ( TouchVerifyCoef() )
		{
		  xx = ( x / touchCoefs.ax ) + touchCoefs.bx;
		  xx = xx - (xx/TOUCH_NON_LINEAR_X);
		  yy = ( y / touchCoefs.ay ) + touchCoefs.by;
		  yy = (uint16_t)( yy - (yy/TOUCH_NON_LINEAR_Y));
		  if (xx > LCD_PIXEL_WIDTH)
			*px = LCD_PIXEL_WIDTH;
		  else
			 *px = xx;
		  if (yy > LCD_PIXEL_HEIGHT)
			 *py = LCD_PIXEL_HEIGHT;
		  else
			 *py = yy;
		}
		else
		{
			*px = x;
			*py = y;
		}
		dataRDY = false;
		if (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_0) == 0) TPReceiveX();
		else EXTI_vSetPB0_ActionEnable(true);
		return true;
	}
	else
	{
		*px = xx;
		*py = yy;
		return false;
	}

}

void TouchReadStore ( void )
{
	bool flag;
	uint16_t x,y;
	GUI_PID_STATE TS_State;

	flag = TouchReadXY ( &x, &y);
	if ( flag )
	{
		TS_State.x = x;
		TS_State.y = y;
		TS_State.Pressed = TOUCH_PRESSED;
		GUI_TOUCH_StoreStateEx(&TS_State);
	} else {
		GUI_TOUCH_GetState(&TS_State);
		if (TS_State.Pressed == TOUCH_PRESSED )
		{
			TS_State.Pressed = TOUCH_UNPRESSED;
			GUI_TOUCH_StoreStateEx(&TS_State);
		}
	}
}

bool isTouch ( void )
{
	bool res;
    if ( GPIO_ReadInputDataBit ( TOUCH_IRQ_PORT, TOUCH_IRQ_PIN ) == RESET )
    	res =  true;
    else res = false;

	return res;
}

void TouchSetCoef ( int16_t _ax, int16_t _bx, int16_t _ay, int16_t _by, uint8_t isReadCorrected )
{
	touchCoefs.ax = _ax;
	touchCoefs.bx = _bx;
	touchCoefs.ay = _ay;
	touchCoefs.by = _by;
	touchCoefs.isReadCorrected = isReadCorrected;
}

bool TouchVerifyCoef(void)
{
	if ( touchCoefs.ax == 0xFFFF || touchCoefs.bx == 0xFFFF
			|| touchCoefs.ay == 0xFFFF || touchCoefs.by == 0xFFFF
			|| touchCoefs.isReadCorrected !=TOUCH_READ_CORRECTED)
		return false;

	return true;
}

void DrawTarget(int x, int y)
{
	int i;

	for (i = 1; i < 5; i++)
	{
		GUI_DrawCircle(x,y,i*5);
	}
}

void TouchCalibrate ( void )
{
	uint16_t x, y, cnt, xL, yL;
	int16_t axc[4], ayc[4], bxc[4], byc[4];
	x = 0; y = 0;

	TouchvInitCalibrate();

	// left top corner draw
	GUI_Clear();
	xL = LCD_GetXSize() / 2;
	yL = LCD_GetYSize() / 3;
	GUI_SetFont(GUI_FONT_COMIC24B_ASCII);
	GUI_DispStringHCenterAt("Calibration", xL, yL);

	for (cnt = 0; cnt < 4; cnt++)
	{
		GUI_SetColor(GUI_WHITE);
		DrawTarget(xCenter[cnt], yCenter[cnt]);
		//GUI_DispStringHCenterAt("Push", xL, yL + 20);
		while (1)
		{
			while ( !isTouch ( ) );
			TouchReadXY ( &x, &y);
			if (x < 4095 && y < 4095)
			{
				xPos[cnt] = x;
				yPos[cnt] = y;
				break;
			}
		}

		//GUI_DispStringHCenterAt("Pull", xL, yL + 20);

		while ( isTouch() );
		GUI_SetColor(GUI_BLACK);
		DrawTarget(xCenter[cnt], yCenter[cnt]);
		GUI_SetColor(GUI_WHITE);
		GUI_DispDecAt(x, xL, yL + (cnt+1)*40, 4);
		GUI_DispDecAt(y, xL, yL + (cnt+1)*60, 4);
	}



	axc[0] = (xPos[3] - xPos[0])/(xCenter[3] - xCenter[0]);
	bxc[0] = xCenter[0] - xPos[0]/axc[0];
	ayc[0] = (yPos[3] - yPos[0])/(yCenter[3] - yCenter[0]);
	byc[0] = yCenter[0] - yPos[0]/ayc[0];

	TouchSetCoef ( axc[0], bxc[0], ayc[0], byc[0],  TOUCH_READ_CORRECTED);
	GUI_Clear();
	GUI_SetColor(GUI_WHITE);
	GUI_DispStringHCenterAt("Calibration Complete", xL, yL);
	GUI_Delay(1000);
	GUI_Clear();
	TouchInit();
}
