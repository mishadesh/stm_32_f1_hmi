#include "ssd1963_driver.h"


uint8_t SSD1963_vInit()
{
	ssd1963_reset();
	vDelayMsCycle(200);
	SSD1963_startup();
	vDelayMsCycle(50);
	int status = ssd1963_low_test();
	if (!status) return SSD1963_STATUS_BAD;
	return SSD1963_STATUS_OK;

}

void ssd1963_reset(void)
{
    GPIO_ResetBits(RESET_PORT, RESET_PIN);
    vDelayMsCycle(10);
    GPIO_SetBits(RESET_PORT, RESET_PIN);
}

uint16_t ssd1963_read_data(void)
{
    uint16_t a =  * (uint16_t *)LCD_DATA;
    return (a);
}
//-------------------------------------------------------------------------------------------------
// ������ �������
//-------------------------------------------------------------------------------------------------
uint16_t ssd1963_read_command(void)
{
    uint16_t a =  * (uint16_t *)LCD_REG;
    return (a);
}

void ssd1963_write_command(uint16_t commandToWrite)
{
    *(uint16_t *) (LCD_REG) = commandToWrite;
}
//-------------------------------------------------------------------------------------------------
// ������ ������
//-------------------------------------------------------------------------------------------------
void ssd1963_write_data(uint16_t dataToWrite)
{
   *(uint16_t *) (LCD_DATA) = dataToWrite;
}


void SSD1963_startup()
{
    ssd1963_write_command(SSD1963_CMD_SET_DISPLAY_OFF); // Turn off display

    ssd1963_write_command(SSD1963_CMD_SET_PLL);
    ssd1963_write_data(0x00); // Disable PLL

    // Set MN(multipliers) of PLL, VCO = crystal freq * (N+1)
    // PLL freq = VCO/M with 250MHz < VCO < 800MHz
    // The max PLL freq is around 120MHz. To obtain 120MHz as the PLL freq
    ssd1963_write_command(SSD1963_CMD_SET_PLL_MN); // Set PLL with OSC = 10MHz (hardware)

    ssd1963_write_data(0x23);
    ssd1963_write_data(0x02); // Divider M = 2, PLL = 360/(M+1) = 120MHz
    ssd1963_write_data(0x54); // Validate M and N values


    ssd1963_write_command(SSD1963_CMD_SET_PLL); // Start PLL command
    ssd1963_write_data(0x01); // enable PLL

   vDelayMsCycle(1);

    ssd1963_write_command(SSD1963_CMD_SET_PLL); // Start PLL command again
    ssd1963_write_data(0x03); // now, use PLL output as system clock

    vDelayMsCycle(10);

    ssd1963_write_command(SSD1963_CMD_SOFT_RESET); // Soft reset

    vDelayMsCycle(100);

    // Set LSHIFT freq, i.e. the DCLK with PLL freq 120MHz set previously
    // Typical DCLK for TY700TFT800480 is 33.3MHz(datasheet), experiment shows 30MHz gives a stable result
    // 30MHz = 120MHz * (LCDC_FPR + 1) / 2^20
    // LCDC_FPR = 262143 (0x3FFFF)
    // Time per line = (DISP_HOR_RESOLUTION + DISP_HOR_PULSE_WIDTH+DISP_HOR_BACK_PORCH + DISP_HOR_FRONT_PORCH) / 30 us = 1056 / 30 = 35.2us
    ssd1963_write_command(SSD1963_CMD_GET_LSHIFT_FREQ);
    ssd1963_write_data(0x04);
    ssd1963_write_data(0xff);
    ssd1963_write_data(0xff);


    //Set panel mode, varies from individual manufacturer
    ssd1963_write_command(SSD1963_CMD_SET_LCD_MODE);
    ssd1963_write_data(0x10); // FRC or dithering
    ssd1963_write_data(0x80); // set TTL mode
    ssd1963_write_data((DISP_HOR_RESOLUTION-1)>>8); //Set panel size
    ssd1963_write_data(DISP_HOR_RESOLUTION-1);
    ssd1963_write_data((DISP_VER_RESOLUTION-1)>>8);
    ssd1963_write_data(DISP_VER_RESOLUTION-1);
    ssd1963_write_data(0x00); //RGB sequence

    //Set horizontal period
    ssd1963_write_command(SSD1963_CMD_SET_HORIZ_PERIOD);
#define HT (DISP_HOR_RESOLUTION + DISP_HOR_PULSE_WIDTH + DISP_HOR_BACK_PORCH + DISP_HOR_FRONT_PORCH)
    ssd1963_write_data((HT - 1) >> 8);
    ssd1963_write_data(HT - 1);
#define HPS (DISP_HOR_PULSE_WIDTH + DISP_HOR_BACK_PORCH)
    ssd1963_write_data((HPS - 1) >> 8);
    ssd1963_write_data(HPS - 1);
    ssd1963_write_data(DISP_HOR_PULSE_WIDTH - 1);
    ssd1963_write_data(0x00);
    ssd1963_write_data(0x00);
    ssd1963_write_data(0x00);

    //Set vertical period
    ssd1963_write_command(SSD1963_CMD_SET_VERT_PERIOD);
#define VT (DISP_VER_PULSE_WIDTH + DISP_VER_BACK_PORCH + DISP_VER_FRONT_PORCH + DISP_VER_RESOLUTION)
    ssd1963_write_data((VT - 1) >> 8);
    ssd1963_write_data(VT - 1);
#define VSP (DISP_VER_PULSE_WIDTH + DISP_VER_BACK_PORCH)
    ssd1963_write_data((VSP - 1) >> 8);
    ssd1963_write_data(VSP - 1);
    ssd1963_write_data(DISP_VER_PULSE_WIDTH - 1);
    ssd1963_write_data(0x00);
    ssd1963_write_data(0x00);

    // Set pixel format, i.e. the bpp
    //ssd1963_write_command(SSD1963_CMD_SET_PIXEL_FORMAT);
    //ssd1963_write_data(0x50); // set 16bpp

    // Rotate screen and rgb
    ssd1963_write_command(SSD1963_CMD_SET_ADDRESS_MODE);
    ssd1963_write_data(0x02); // top to bottom, left to right, normal,  top to bottom, rgb, left to right, normal

    // Set pixel data interface
    ssd1963_write_command(SSD1963_CMD_SET_PIXEL_DATA_INTERFACE);
    ssd1963_write_data(0x03); // 16-bit(565 format)
    //ssd1963_write_data(0x00); // 8-bit data for 16bpp

    // Exit Idle mode
    ssd1963_write_command(SSD1963_CMD_EXIT_IDLE_MODE);

    ssd1963_write_command(SSD1963_CMD_SET_DISPLAY_ON);

}

int ssd1963_low_test(void)
{
    uint16_t supplierId;
    uint8_t productId, revisionId;

    ssd1963_write_command(SSD1963_CMD_READ_DDB);

    supplierId = ssd1963_read_data() << 8;
    supplierId |= (ssd1963_read_data() & 0xff);

    productId = ssd1963_read_data();
    revisionId = ssd1963_read_data();

    // ���� �� ���������������
    if (supplierId != 0x0157 || productId != 0x61 || revisionId != 0x01) {
        return (0);
    }

    // ���� �� ����� ������
    // ������ �� 0-� ������� 0-� ��������, ��� ������ ������ �������� 0...15 �������
    ssd1963_write_command(SSD1963_CMD_SET_COLUMN_ADDRESS);
    ssd1963_write_data(0);
    ssd1963_write_data(0);
    ssd1963_write_data(0);
    ssd1963_write_data(16);

    ssd1963_write_command(SSD1963_CMD_SET_PAGE_ADDRESS);
    ssd1963_write_data(0);
    ssd1963_write_data(0);
    ssd1963_write_data(0);
    ssd1963_write_data(0);

    ssd1963_write_command(SSD1963_CMD_WRITE_MEMORY_START);
    for (unsigned int i = 0; i < 16; i++)
    {
        // ����� ��� �������� ���� ������
        uint16_t mask = 0x0001 << i;
        ssd1963_write_data(mask);
    }

    // ������ �� 0-� ������� 0-� ��������, ��� ������� ������ �������� 0...15 �������
    ssd1963_write_command(SSD1963_CMD_SET_COLUMN_ADDRESS);
    ssd1963_write_data(0);
    ssd1963_write_data(0);
    ssd1963_write_data(0);
    ssd1963_write_data(16);

    ssd1963_write_command(SSD1963_CMD_SET_PAGE_ADDRESS);
    ssd1963_write_data(0);
    ssd1963_write_data(0);
    ssd1963_write_data(0);
    ssd1963_write_data(0);

    ssd1963_write_command(SSD1963_CMD_READ_MEMORY_START);
    for (unsigned int i = 0; i < 16; i++)
    {
        // ����� ��� �������� ���� ������
        uint16_t mask = 0x0001 << i;
        uint16_t readed = ssd1963_read_data();
        if (readed != mask) {
            return (0);
        }
    }

    return (1);
}

void ssd1963_set_area(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1)
{
    //long offset;
    //offset = (uint16_t)_activePage*(GetMaxY()+1);
    //start_y = offset + start_y;
    //end_y   = offset + end_y;

    ssd1963_write_command(SSD1963_CMD_SET_COLUMN_ADDRESS);
    ssd1963_write_data(x0 >> 8);
    ssd1963_write_data(x0 & 0xff);
    ssd1963_write_data(x1 >> 8);
    ssd1963_write_data(x1 & 0xff);

    ssd1963_write_command(SSD1963_CMD_SET_PAGE_ADDRESS);
    ssd1963_write_data(y0 >> 8);
    ssd1963_write_data(y0 & 0xff);
    ssd1963_write_data(y1 >> 8);
    ssd1963_write_data(y1 & 0xff);
}

void ssd1963_set_bright(uint8_t level)
{
    uint8_t brightness =1;
    for(uint8_t i=1;i<=level;i++)
        brightness *=2;
    ssd1963_write_command(SSD1963_CMD_SET_PWM_CONF);
    ssd1963_write_data(0x01);
    ssd1963_write_data(brightness-1);
    ssd1963_write_data(0x01);

}
