#ifndef SSD1963_DRIVER_H_INCLUDED
#define SSD1963_DRIVER_H_INCLUDED
#include <SSD1963_config.h>
#include "stm32f10x.h"
#include "ssd1963-command-set.h"
#include "Delay.h"

#define SSD1963_STATUS_BAD 1
#define SSD1963_STATUS_OK  0


void ssd1963_set_bright(uint8_t level);
void ssd1963_set_area(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1);
int ssd1963_low_test(void);
void SSD1963_startup();
void ssd1963_write_data(uint16_t dataToWrite);
void ssd1963_write_command(uint16_t commandToWrite);
uint16_t ssd1963_read_command(void);
uint16_t ssd1963_read_data(void);
void ssd1963_reset(void);
uint8_t SSD1963_vInit();

#endif /* SSD1963_DRIVER_H_INCLUDED */
