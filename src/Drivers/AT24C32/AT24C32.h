/*
 * AT24C32.h
 *
 *  Created on: 21 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef DRIVERS_AT24C32_INC_AT24C32_H_
#define DRIVERS_AT24C32_INC_AT24C32_H_

#include <System_config.h>
#include "I2C.h"

#define EEPROM_HW_ADDRESS  	0x50
#define EEPROM_I2C 			I2C1

uint8_t AT24C32_ReadByte(uint16_t reg_address);
void AT24C32_WriteByte(uint16_t reg_address, uint8_t data);
void AT24C32_WriteWord(uint16_t reg_address,  uint16_t data);
uint16_t AT24C32_ReadWord(uint16_t reg_address);

#endif /* DRIVERS_AT24C32_INC_AT24C32_H_ */


