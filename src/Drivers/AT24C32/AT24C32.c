/*
 * AT24C32.c
 *
 *  Created on: 21 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#include "AT24C32.h"


uint8_t AT24C32_ReadByte(uint16_t reg_address)
{
	return I2C_ReadByte16bitAddr(EEPROM_I2C, (uint8_t)EEPROM_HW_ADDRESS, reg_address);
}

void AT24C32_WriteByte(uint16_t reg_address,  uint8_t data)
{
	I2C_WriteByte16bitAddr(EEPROM_I2C, (uint8_t)EEPROM_HW_ADDRESS, reg_address, data);
	vTaskDelay(10);
}

void AT24C32_WriteWord(uint16_t reg_address,  uint16_t data)
{
	I2C_WriteByte16bitAddr(EEPROM_I2C, (uint8_t)EEPROM_HW_ADDRESS, reg_address, (uint8_t)(data&0x00FF));
	vTaskDelay(10);
	I2C_WriteByte16bitAddr(EEPROM_I2C, (uint8_t)EEPROM_HW_ADDRESS, reg_address+1, (uint8_t)((data&0xFF00)>>8));
	vTaskDelay(10);
}

uint16_t AT24C32_ReadWord(uint16_t reg_address)
{
	uint16_t data;
	data = I2C_ReadByte16bitAddr(EEPROM_I2C, (uint8_t)EEPROM_HW_ADDRESS, reg_address);
	data |= ((uint16_t)(I2C_ReadByte16bitAddr(EEPROM_I2C, (uint8_t)EEPROM_HW_ADDRESS, reg_address+1)))<<8;
	return data;
}
