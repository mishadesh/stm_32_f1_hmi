/*
 * DMA.c
 *
 *  Created on: 18 ��� 2019 �.
 *      Author: Mishadesh-x12
 */
#include "DMA.h"
#include "TouchPanel.h"

void DMA1_Channel2_IRQHandler(void);
void DMA1_Channel4_IRQHandler(void);
void DMA1_Channel5_IRQHandler(void);
void DMA1_Channel7_IRQHandler(void);

/*
void DMA1_UART1_Init(uint8_t* Rx_Buf_Addr, uint8_t* Tx_Buf_Addr, uint8_t RxTxbufSize)
{
	RCC_Init_DMA1();
	DMA_InitTypeDef DMA_InitStructTx, DMA_InitStructRx;
	DMA_InitStructTx.DMA_PeripheralBaseAddr = (uint32_t)&(USART1->DR);
	DMA_InitStructTx.DMA_MemoryBaseAddr = (uint32_t)Tx_Buf_Addr;
	DMA_InitStructTx.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStructTx.DMA_BufferSize = RxTxbufSize;
	DMA_InitStructTx.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructTx.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructTx.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructTx.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructTx.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructTx.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructTx.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel4, &DMA_InitStructTx);

	DMA_InitStructRx.DMA_PeripheralBaseAddr = (uint32_t)&(USART1->DR);
	DMA_InitStructRx.DMA_MemoryBaseAddr = (uint32_t)Rx_Buf_Addr;
	DMA_InitStructRx.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructRx.DMA_BufferSize = RxTxbufSize;
	DMA_InitStructRx.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructRx.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructRx.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructRx.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructRx.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructRx.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructRx.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel5, &DMA_InitStructRx);


	DMA_ITConfig(DMA1_Channel4, DMA_IT_TC, ENABLE);
	NVIC_DMA_1_USART1_Init();
}
*/
/*
void DMA1_Channel4_IRQHandler(void)
{
    DMA_Cmd(DMA1_Channel4, DISABLE);
    DMA_ClearITPendingBit(DMA1_IT_TC4);
}
*/

void DMA1_UART2_Init(uint8_t* Rx_Buf_Addr, uint8_t* Tx_Buf_Addr, uint8_t RxTxbufSize)
{
	RCC_Init_DMA1();
	DMA_InitTypeDef DMA_InitStructTx, DMA_InitStructRx;
	DMA_InitStructTx.DMA_PeripheralBaseAddr = (uint32_t)&(USART2->DR);
	DMA_InitStructTx.DMA_MemoryBaseAddr = (uint32_t)Tx_Buf_Addr;//(uint32_t)&DMA_USART2_TX_BUF[0];
	DMA_InitStructTx.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStructTx.DMA_BufferSize = RxTxbufSize;
	DMA_InitStructTx.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructTx.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructTx.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructTx.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructTx.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructTx.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructTx.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel7, &DMA_InitStructTx);

	DMA_InitStructRx.DMA_PeripheralBaseAddr = (uint32_t)&(USART2->DR);
	DMA_InitStructRx.DMA_MemoryBaseAddr = (uint32_t)Rx_Buf_Addr;//(uint32_t)&DMA_USART2_RX_BUF[0];
	DMA_InitStructRx.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructRx.DMA_BufferSize = RxTxbufSize;
	DMA_InitStructRx.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructRx.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructRx.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructRx.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructRx.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructRx.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructRx.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel6, &DMA_InitStructRx);


	DMA_ITConfig(DMA1_Channel7, DMA_IT_TC, ENABLE);
	NVIC_DMA_1_USART2_Init();
}

void DMA1_UART3_Init(uint8_t* Rx_Buf_Addr, uint8_t* Tx_Buf_Addr, uint8_t RxTxbufSize)
{
	RCC_Init_DMA1();
	DMA_InitTypeDef DMA_InitStructTx, DMA_InitStructRx;
	DMA_InitStructTx.DMA_PeripheralBaseAddr = (uint32_t)&(USART3->DR);
	DMA_InitStructTx.DMA_MemoryBaseAddr = (uint32_t)Tx_Buf_Addr;//(uint32_t)&DMA_USART2_TX_BUF[0];
	DMA_InitStructTx.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStructTx.DMA_BufferSize = RxTxbufSize;
	DMA_InitStructTx.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructTx.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructTx.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructTx.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructTx.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructTx.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructTx.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel2, &DMA_InitStructTx);

	DMA_InitStructRx.DMA_PeripheralBaseAddr = (uint32_t)&(USART3->DR);
	DMA_InitStructRx.DMA_MemoryBaseAddr = (uint32_t)Rx_Buf_Addr;//(uint32_t)&DMA_USART2_RX_BUF[0];
	DMA_InitStructRx.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructRx.DMA_BufferSize = RxTxbufSize;
	DMA_InitStructRx.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructRx.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructRx.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructRx.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructRx.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructRx.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructRx.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel3, &DMA_InitStructRx);


	DMA_ITConfig(DMA1_Channel2, DMA_IT_TC, ENABLE);
	NVIC_DMA_1_USART3_Init();
}

void DMA1_Channel7_IRQHandler(void)
{
    DMA_Cmd(DMA1_Channel7, DISABLE);
    DMA_ClearITPendingBit(DMA1_IT_TC7);
}

void DMA1_Channel2_IRQHandler(void)
{
    DMA_Cmd(DMA1_Channel2, DISABLE);
    DMA_ClearITPendingBit(DMA1_IT_TC2);
}

void DMA1_SPI2_Init(uint8_t* Rx_Buf_Addr, uint8_t* Tx_Buf_Addr, uint8_t RxTxbufSize)
{
	RCC_Init_DMA1();
	DMA_InitTypeDef DMA_InitStructTx, DMA_InitStructRx;

	DMA_InitStructTx.DMA_PeripheralBaseAddr = (uint32_t)&(SPI2->DR);
	DMA_InitStructTx.DMA_MemoryBaseAddr = (uint32_t)Tx_Buf_Addr;//(uint32_t)&DMA_USART2_TX_BUF[0];
	DMA_InitStructTx.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStructTx.DMA_BufferSize = RxTxbufSize;
	DMA_InitStructTx.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructTx.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructTx.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructTx.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructTx.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructTx.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructTx.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel5, &DMA_InitStructTx);

	DMA_InitStructRx.DMA_PeripheralBaseAddr = (uint32_t)&(SPI2->DR);
	DMA_InitStructRx.DMA_MemoryBaseAddr = (uint32_t)Rx_Buf_Addr;//(uint32_t)&DMA_USART2_RX_BUF[0];
	DMA_InitStructRx.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructRx.DMA_BufferSize = RxTxbufSize;
	DMA_InitStructRx.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructRx.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructRx.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructRx.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructRx.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructRx.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructRx.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel4, &DMA_InitStructRx);
	DMA_ITConfig(DMA1_Channel4, DMA_IT_TC, ENABLE);
	DMA_ITConfig(DMA1_Channel5, DMA_IT_TC, ENABLE);

	NVIC_DMA_1_SPI2_Init();
}

void DMA1_Channel5_IRQHandler(void)
{
    DMA_Cmd(DMA1_Channel5, DISABLE);
    DMA_ClearITPendingBit(DMA1_IT_TC5);
}

void DMA1_Channel4_IRQHandler(void)
{
    DMA_Cmd(DMA1_Channel4, DISABLE);
    DMA_ClearITPendingBit(DMA1_IT_TC4);
    TPGetXData();
}

void DMA1_ADC1_Init(uint16_t* ConvBuffAddr, uint8_t ConvBuffSize)
{
	RCC_Init_DMA1();
	DMA_InitTypeDef DMA_InitStructure;

    DMA_InitStructure.DMA_BufferSize = ConvBuffSize;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)ConvBuffAddr;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&ADC1->DR;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_Priority = DMA_Priority_Low;
    DMA_Init(DMA1_Channel1, &DMA_InitStructure);
    DMA_Cmd(DMA1_Channel1 , ENABLE ) ;
}
