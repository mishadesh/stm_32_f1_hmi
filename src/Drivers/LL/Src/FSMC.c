/*
 * FSMC_Init.c
 *
 *  Created on: 29 ���. 2019 �.
 *      Author: Mishadesh-x12
 */
#include <FSMC.h>

void FSMC_Init()
{
	RCC_Init_FSMC();
	GPIO_Init_FSMC_Pinout();

	FSMC_NORSRAMInitTypeDef fsmc;
	FSMC_NORSRAMTimingInitTypeDef fsmcTiming;

	fsmcTiming.FSMC_AddressSetupTime = 1;//5
	fsmcTiming.FSMC_AddressHoldTime = 0x00;
	fsmcTiming.FSMC_DataSetupTime = 1;//20
	fsmcTiming.FSMC_BusTurnAroundDuration = 0x00;
	fsmcTiming.FSMC_CLKDivision = 0x00;
	fsmcTiming.FSMC_DataLatency = 0x00;
	fsmcTiming.FSMC_AccessMode = FSMC_AccessMode_A;
	fsmc.FSMC_Bank = FSMC_Bank1_NORSRAM1;
	fsmc.FSMC_AsynchronousWait = FSMC_AsynchronousWait_Disable;
	fsmc.FSMC_DataAddressMux = FSMC_DataAddressMux_Disable;
	fsmc.FSMC_MemoryType = FSMC_MemoryType_SRAM;
	fsmc.FSMC_MemoryDataWidth = FSMC_MemoryDataWidth_16b;
	fsmc.FSMC_BurstAccessMode = FSMC_BurstAccessMode_Disable;
	fsmc.FSMC_WaitSignalPolarity = FSMC_WaitSignalPolarity_Low;
	fsmc.FSMC_WrapMode = FSMC_WrapMode_Disable;
	fsmc.FSMC_WaitSignalActive = FSMC_WaitSignalActive_BeforeWaitState;
	fsmc.FSMC_WriteOperation = FSMC_WriteOperation_Enable;
	fsmc.FSMC_WaitSignal = FSMC_WaitSignal_Disable;
	fsmc.FSMC_ExtendedMode = FSMC_ExtendedMode_Disable;
	fsmc.FSMC_WriteBurst = FSMC_WriteBurst_Disable;
	fsmc.FSMC_ReadWriteTimingStruct = &fsmcTiming;
	fsmc.FSMC_WriteTimingStruct = &fsmcTiming;

	FSMC_NORSRAMInit(&fsmc);
	FSMC_NORSRAMCmd(FSMC_Bank1_NORSRAM1, ENABLE);
}

