/*
 * I2C.c
 *
 *  Created on: 20 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#include "I2C.h"

#define __HAL_RCC_I2C1_FORCE_RESET()       (RCC->APB1RSTR |= (RCC_APB1RSTR_I2C1RST))
#define __HAL_RCC_I2C1_RELEASE_RESET()     (RCC->APB1RSTR &= ~(RCC_APB1RSTR_I2C1RST))

void I2C1_Init()
{
	GPIO_Init_I2C1_Pinout();
	RCC_Init_I2C1();
	__HAL_RCC_I2C1_FORCE_RESET();
	int n = 100000;
	while(n>0) n--;
	__HAL_RCC_I2C1_RELEASE_RESET();
	 n = 100000;
	while(n>0) n--;
	I2C_InitTypeDef I2C1interface;
	I2C1interface.I2C_Mode = I2C_Mode_I2C;
	I2C1interface.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C1interface.I2C_OwnAddress1 = 0;
	I2C1interface.I2C_Ack = I2C_Ack_Disable;
	I2C1interface.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C1interface.I2C_ClockSpeed = 100000;
	I2C_Init(I2C1, &I2C1interface);
	I2C_Cmd(I2C1, ENABLE);
}

void I2C_StartTransmission(I2C_TypeDef* I2Cx, uint8_t transmissionDirection,  uint8_t slaveAddress)
{
    // �� ������ ������ ����, ���� ���� ������������
    while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY));
    // ���������� ����� - ��� ��� ������� )
    I2C_GenerateSTART(I2Cx, ENABLE);
    // ���� ���� ������� ������ ����
    while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT));
    // �������� ����� ������������  //�������� ��� ����� ����� �����  //���� �� ���������� - ��, ����� ����� �����
    //http://microtechnics.ru/stm32-ispolzovanie-i2c/#comment-8109
    I2C_Send7bitAddress(I2Cx, slaveAddress<<1, transmissionDirection);
    // � ������ � ��� ��� �������� �������� ������� - � ����������� �� ���������� ����������� ������ �������
    if(transmissionDirection== I2C_Direction_Transmitter)
    {
    	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
    }
    if(transmissionDirection== I2C_Direction_Receiver)
    {
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));
    }
}

/*******************************************************************/
void I2C_WriteData(I2C_TypeDef* I2Cx, uint8_t data)
{
    // ������ �������� �������� ������� �� SPL � ����, ���� ������ ������
    I2C_SendData(I2Cx, data);
    while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
}



/*******************************************************************/
uint8_t I2C_ReadData(I2C_TypeDef* I2Cx)
{
    // ��� ������� ������, ��� ������ ������ ������ ���������� ��������� �� � ����������
    while( !I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED) );
    uint8_t data;
    data = I2C_ReceiveData(I2Cx);
    return data;
}

void I2C_StopTransmission(I2C_TypeDef* I2Cx)
{
	I2C_GenerateSTOP(I2Cx, ENABLE);
}

uint8_t I2C_ReadByte8bitAddr(I2C_TypeDef* I2Cx, uint8_t slaveAddress, uint8_t reg_address)
{
	uint8_t data;
	I2C_StartTransmission(I2Cx, I2C_Direction_Transmitter, slaveAddress);
	I2C_WriteData(I2Cx, reg_address);
	I2C_StopTransmission(I2Cx);
	I2C_StartTransmission(I2Cx, I2C_Direction_Receiver, slaveAddress);
	data = I2C_ReadData(I2Cx);
	I2C_StopTransmission(I2Cx);
	return data;
}

void I2C_WriteByte8bitAddr(I2C_TypeDef* I2Cx, uint8_t slaveAddress, uint8_t reg_address, uint8_t data)
{
	I2C_StartTransmission(I2Cx, I2C_Direction_Transmitter, slaveAddress);
	I2C_WriteData(I2Cx, reg_address);
	I2C_WriteData(I2Cx, data);
	I2C_StopTransmission(I2Cx);
}

uint8_t I2C_ReadByte16bitAddr(I2C_TypeDef* I2Cx, uint8_t slaveAddress, uint16_t reg_address)
{
	uint8_t data;
	I2C_StartTransmission(I2Cx, I2C_Direction_Transmitter, slaveAddress);
	I2C_WriteData(I2Cx, (uint8_t)((reg_address&0xFF00)>>8));
	I2C_WriteData(I2Cx, (uint8_t)(reg_address&0x00FF));
	I2C_StopTransmission(I2Cx);
	I2C_StartTransmission(I2Cx, I2C_Direction_Receiver, slaveAddress);
	data = I2C_ReadData(I2Cx);
	I2C_StopTransmission(I2Cx);
	return data;
}

void I2C_WriteByte16bitAddr(I2C_TypeDef* I2Cx, uint8_t slaveAddress, uint16_t reg_address, uint8_t data)
{
	I2C_StartTransmission(I2Cx, I2C_Direction_Transmitter, slaveAddress);
	I2C_WriteData(I2Cx, (uint8_t)((reg_address&0xFF00)>>8));
	I2C_WriteData(I2Cx, (uint8_t)(reg_address&0x00FF));
	I2C_WriteData(I2Cx, data);
	I2C_StopTransmission(I2Cx);
}
