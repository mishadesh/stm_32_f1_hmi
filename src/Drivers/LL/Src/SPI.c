/*
 * SPI_Init.c
 *
 *  Created on: 29 ���. 2019 �.
 *      Author: Mishadesh-x12
 */

#include <SPI.h>
#include "TouchPanel.h"

uint8_t SPI_DATA_RX[3];
uint8_t SPI_DATA_TX[3];

uint8_t * SPI2_GetRxBufAddr()
{
	return SPI_DATA_RX;
}
uint8_t * SPI2_GetTxBufAddr()
{
	return SPI_DATA_TX;
}


void SPI_vInit_SPI2()
{
	RCC_Init_SPI2();
	GPIO_Init_SPI2();
	GPIO_Init_TouchPanel();
	SPI_I2S_DeInit(SPI2);
	SPI_InitTypeDef  SPI_InitStructure;

	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	//SPI_InitStructure.SPI_DataSize = SPI_DataSize_16b;
	 SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init ( SPI2, &SPI_InitStructure );
	SPI_CalculateCRC ( SPI2, DISABLE );

	SPI_SSOutputCmd ( SPI2, DISABLE);
	SPI_Cmd ( SPI2, ENABLE );
	SPI_NSSInternalSoftwareConfig ( SPI2, SPI_NSSInternalSoft_Set );
}

void SPI2_DMA_Enable()
{
	SPI_I2S_DMACmd(SPI2, SPI_I2S_DMAReq_Rx, ENABLE );
	SPI_I2S_DMACmd(SPI2, SPI_I2S_DMAReq_Tx, ENABLE );
}

void SPI2_DMA_Disable()
{
	SPI_I2S_DMACmd(SPI2, SPI_I2S_DMAReq_Rx, DISABLE );
	SPI_I2S_DMACmd(SPI2, SPI_I2S_DMAReq_Tx, DISABLE );
}

void SPI2_DMA_Send(uint8_t* data, uint8_t len)
{
	T_CS ( );
	memcpy(SPI_DATA_TX, data, len);
	DMA_SetCurrDataCounter(DMA1_Channel4, 3);
	DMA_SetCurrDataCounter(DMA1_Channel5, 3);
	DMA_Cmd(DMA1_Channel4, ENABLE);
	DMA_Cmd(DMA1_Channel5, ENABLE);

}

static void SpiDelay ( uint16_t DelayCnt )
{
	uint16_t i;
	for ( i = 0; i < DelayCnt; i ++ );
}

uint16_t SPI_Touch_Write ( uint8_t cmd, SPI_TypeDef* port )
{
	uint16_t dat, MSB, LSB;

	SPI_I2S_SendData ( port, cmd );
	// Wait to receive a byte
	while ( SPI_I2S_GetFlagStatus ( port, SPI_I2S_FLAG_RXNE ) == RESET ) { ; }
	SPI_I2S_ReceiveData ( port );
	SPI_I2S_SendData ( port, 0 );
	while ( SPI_I2S_GetFlagStatus ( port, SPI_I2S_FLAG_RXNE ) == RESET ) { ; }
	MSB = SPI_I2S_ReceiveData ( port );
	SPI_I2S_SendData ( port, 0 );
	while ( SPI_I2S_GetFlagStatus ( port, SPI_I2S_FLAG_RXNE ) == RESET ) { ; }
	// Return the byte read from the SPI bus
	LSB = SPI_I2S_ReceiveData ( port );
	dat = (((MSB&0x00FF)<<8) | (LSB))>>3;

	SpiDelay ( 10 );
	return dat;
}
