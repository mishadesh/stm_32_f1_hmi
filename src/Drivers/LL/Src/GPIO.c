#include <GPIO.h>

void GPIO_Init_USART2_MANU_RTS()
{
	RCC_Init_Periph(RCC_GPIOA);
	GPIO_InitTypeDef PA1;
	PA1.GPIO_Mode = GPIO_Mode_Out_PP;
	PA1.GPIO_Pin = GPIO_Pin_1;
	PA1.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &PA1);
}

void GPIO_Init_PD6_Led()
{
	RCC_Init_Periph(RCC_GPIOD);
	GPIO_InitTypeDef PD6;
	PD6.GPIO_Mode = GPIO_Mode_Out_PP;
	PD6.GPIO_Pin = GPIO_Pin_6;
	PD6.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOD, &PD6);
}

void GPIO_Init_PC6_Led()
{
	RCC_Init_Periph(RCC_GPIOC);
	GPIO_InitTypeDef PC6;
	PC6.GPIO_Mode = GPIO_Mode_Out_PP;
	PC6.GPIO_Pin = GPIO_Pin_6;
	PC6.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOC, &PC6);
}

void GPIO_Init_USART_1()
{
	RCC_Init_Periph(RCC_GPIOA);
	GPIO_InitTypeDef usart1;
	usart1.GPIO_Pin = (GPIO_Pin_9);
	usart1.GPIO_Mode = GPIO_Mode_AF_PP;
	usart1.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &usart1);
	usart1.GPIO_Pin = (GPIO_Pin_10);
	usart1.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &usart1);
}

void GPIO_Init_USART_2()
{
	RCC_Init_Periph(RCC_GPIOA);
	GPIO_InitTypeDef usart2;
	usart2.GPIO_Pin = (GPIO_Pin_2);
	usart2.GPIO_Mode = GPIO_Mode_AF_PP;
	usart2.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &usart2);
	usart2.GPIO_Pin = (GPIO_Pin_3);
	usart2.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &usart2);
}

void GPIO_USART3_Init()
{
	RCC_Init_Periph(RCC_GPIOB);
	GPIO_InitTypeDef usart3;
	usart3.GPIO_Pin = (GPIO_Pin_10);
	usart3.GPIO_Mode = GPIO_Mode_AF_PP;
	usart3.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOB, &usart3);
	usart3.GPIO_Pin = (GPIO_Pin_11);
	usart3.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOB, &usart3);
}

void GPIO_Init_SPI2()
{
	RCC_Init_Periph(RCC_GPIOB);
	GPIO_InitTypeDef PB_SPI2;

	PB_SPI2.GPIO_Pin = (GPIO_Pin_13 | GPIO_Pin_15);
	PB_SPI2.GPIO_Speed = GPIO_Speed_50MHz;
	PB_SPI2.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOB, &PB_SPI2);

	PB_SPI2.GPIO_Pin = 0;

	PB_SPI2.GPIO_Pin = (GPIO_Pin_14);
	PB_SPI2.GPIO_Speed = GPIO_Speed_50MHz;
	PB_SPI2.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOB, &PB_SPI2);
}

void GPIO_Init_SPI1()
{
	RCC_Init_Periph(RCC_GPIOA);
	GPIO_InitTypeDef PB_SPI1;

	PB_SPI1.GPIO_Pin = (GPIO_Pin_7 | GPIO_Pin_5);
	PB_SPI1.GPIO_Speed = GPIO_Speed_50MHz;
	PB_SPI1.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &PB_SPI1);

	PB_SPI1.GPIO_Pin = 0;

	PB_SPI1.GPIO_Pin = (GPIO_Pin_6);
	PB_SPI1.GPIO_Speed = GPIO_Speed_50MHz;
	PB_SPI1.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &PB_SPI1);
}

void GPIO_Init_TouchPanel()
{
	RCC_Init_Periph(RCC_GPIOB);

	GPIO_InitTypeDef PB_TP;

	PB_TP.GPIO_Pin = (GPIO_Pin_12);
	PB_TP.GPIO_Speed = GPIO_Speed_50MHz;
	PB_TP.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB, &PB_TP);

	PB_TP.GPIO_Pin = 0;

	PB_TP.GPIO_Pin = (GPIO_Pin_0);
	PB_TP.GPIO_Speed = GPIO_Speed_50MHz;
	PB_TP.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOB, &PB_TP);
}

void GPIO_Init_FSMC_Pinout()
{
	//FSMC PORT - Pd and Pe
	//FSMC Pins PD 0,1,4,5,7,8,9,10,11,14,15
	//FSMC Pins PE 7,8,9,10,11,12,13,14,15

	GPIO_InitTypeDef FSMC_PINOUT;

	RCC_Init_Periph(RCC_GPIOD);
	RCC_Init_Periph(RCC_GPIOE);

	FSMC_PINOUT.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_7
		| GPIO_Pin_11| GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_14 | GPIO_Pin_15;
	FSMC_PINOUT.GPIO_Mode = GPIO_Mode_AF_PP;
	FSMC_PINOUT.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &FSMC_PINOUT);

	FSMC_PINOUT.GPIO_Pin = 0;

	FSMC_PINOUT.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10| GPIO_Pin_11 |
		GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14	| GPIO_Pin_15;
	FSMC_PINOUT.GPIO_Mode = GPIO_Mode_AF_PP;
	FSMC_PINOUT.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOE, &FSMC_PINOUT);

	FSMC_PINOUT.GPIO_Pin = 0;

	FSMC_PINOUT.GPIO_Pin = GPIO_Pin_1;
	FSMC_PINOUT.GPIO_Mode = GPIO_Mode_Out_PP;
	FSMC_PINOUT.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOE, &FSMC_PINOUT);

	GPIO_ResetBits(GPIOE, GPIO_Pin_1);
}

void GPIO_Init_I2C1_Pinout()
{
	RCC_Init_Periph(RCC_GPIOB);
	RCC_Init_AFIO();
	GPIO_PinRemapConfig(GPIO_Remap_I2C1, ENABLE);
	GPIO_InitTypeDef I2C1p;
	I2C1p.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
	I2C1p.GPIO_Speed = GPIO_Speed_50MHz;
	I2C1p.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_Init(GPIOB, &I2C1p);
}
void GPIO_Init_I2C2_Pinout()
{
	RCC_Init_Periph(RCC_GPIOB);

	GPIO_InitTypeDef I2C1p;
	I2C1p.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
	I2C1p.GPIO_Speed = GPIO_Speed_50MHz;
	I2C1p.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_Init(GPIOB, &I2C1p);
}

void GPIO_ADC1_PinIn()
{
	RCC_Init_Periph(RCC_GPIOC);
	GPIO_InitTypeDef ADC1pin;
	ADC1pin.GPIO_Mode = GPIO_Mode_AIN;
	ADC1pin.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_Init(GPIOA, &ADC1pin);
}
