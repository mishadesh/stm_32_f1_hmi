/*
 * NVIC.c
 *
 *  Created on: 18 ��� 2019 �.
 *      Author: Mishadesh-x12
 */
#include "NVIC.h"

void NVIC_DMA_1_USART1_Init()
{
	//for OS ISR Interrupts prio range from 11 to 15
	NVIC_SetPriority(USART1_IRQn, 13);
	NVIC_EnableIRQ(USART1_IRQn);
}

void NVIC_DMA_1_USART2_Init()
{
	//for OS ISR Interrupts prio range from 11 to 15
	NVIC_SetPriority(USART2_IRQn, 12);
	NVIC_EnableIRQ(USART2_IRQn);
	NVIC_EnableIRQ(DMA1_Channel7_IRQn);
}

void NVIC_DMA_1_USART3_Init()
{
	//for OS ISR Interrupts prio range from 11 to 15
	NVIC_SetPriority(USART3_IRQn, 15);
	NVIC_EnableIRQ(USART3_IRQn);
	NVIC_EnableIRQ(DMA1_Channel2_IRQn);
}

void NVIC_DMA_1_SPI2_Init()
{
	NVIC_EnableIRQ(DMA1_Channel5_IRQn);
	NVIC_EnableIRQ(DMA1_Channel4_IRQn);
}

void NVIC_EXTI_TOUCH_Init()
{
	NVIC_SetPriority(EXTI0_IRQn, 14);
	NVIC_EnableIRQ(EXTI0_IRQn);
}
