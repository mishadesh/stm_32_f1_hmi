/*
 * ADC.c
 *
 *  Created on: 2 ���. 2019 �.
 *      Author: Mishadesh-x12
 */

#include "ADC.h"

uint16_t ADCBuffer[5] = {0,0,0,0,0};

void ADC1_Init()
{
	ADC_InitTypeDef ADC_InitStructure;

	RCC_Init_ADC1();
	GPIO_ADC1_PinIn();

    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
    ADC_InitStructure.ADC_NbrOfChannel = 5;
    ADC_InitStructure.ADC_ScanConvMode = ENABLE;


    ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 1, ADC_SampleTime_239Cycles5);
    ADC_RegularChannelConfig(ADC1, ADC_Channel_11, 2, ADC_SampleTime_239Cycles5);
    ADC_RegularChannelConfig(ADC1, ADC_Channel_12, 3, ADC_SampleTime_239Cycles5);
    ADC_RegularChannelConfig(ADC1, ADC_Channel_13, 4, ADC_SampleTime_239Cycles5);
    ADC_RegularChannelConfig(ADC1, ADC_Channel_TempSensor, 5, ADC_SampleTime_239Cycles5);

    ADC_Init(ADC1, &ADC_InitStructure);
    ADC_TempSensorVrefintCmd(ENABLE);
    ADC_Cmd(ADC1 , ENABLE ) ;
    ADC_DMACmd(ADC1 , ENABLE ) ;
    ADC_ResetCalibration(ADC1);
    while(ADC_GetResetCalibrationStatus(ADC1));
    ADC_StartCalibration(ADC1);
    while(ADC_GetCalibrationStatus(ADC1));
    ADC_SoftwareStartConvCmd ( ADC1 , ENABLE ) ;

    DMA1_ADC1_Init(ADC1_GetResultBufferAddress(), 5);

}

uint16_t * ADC1_GetResultBufferAddress()
{
	return ADCBuffer;
}
