/*
 * EXTI.c
 *
 *  Created on: 25 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#include "EXTI.h"

void EXTI0_IRQHandler(void);

static bool PB0_ActionisEnabled = true;
static pvPB0ActionCb pvPB0Action;

void EXTI_TOUCH_IRQ_Init()
{
	RCC_Init_AFIO();
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource0);
	EXTI_InitTypeDef TOUCH_IRQ_EXTI_Init;
	TOUCH_IRQ_EXTI_Init.EXTI_Line = EXTI_Line0;
	TOUCH_IRQ_EXTI_Init.EXTI_LineCmd = ENABLE;
	TOUCH_IRQ_EXTI_Init.EXTI_Mode = EXTI_Mode_Interrupt;
	TOUCH_IRQ_EXTI_Init.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_Init(&TOUCH_IRQ_EXTI_Init);
	NVIC_EXTI_TOUCH_Init();
}

bool EXTI_bGetPB0_ActionEnable()
{
	return PB0_ActionisEnabled;
}
void EXTI_vSetPB0_ActionEnable(bool newflag)
{
	PB0_ActionisEnabled = newflag;
}

void EXTI_vRegisterPB0ActionCb(pvPB0ActionCb newCb)
{
	if (newCb != 0)
	pvPB0Action = newCb;
}

void EXTI0_IRQHandler(void) {
    /* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line0) != RESET)
    {
         if (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_0) == 0) {
            // Falling
        	if (PB0_ActionisEnabled)
        	{
        		EXTI_vSetPB0_ActionEnable(false);
        		if (pvPB0Action !=0)
        			pvPB0Action();
        	}
     }

        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line0);
    }
}
