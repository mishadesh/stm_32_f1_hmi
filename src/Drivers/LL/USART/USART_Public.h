/*
 * USART_Public.h
 *
 *  Created on: 8 ���. 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef SRC_DRIVERS_LL_USART_USART_PUBLIC_H_
#define SRC_DRIVERS_LL_USART_USART_PUBLIC_H_

typedef enum
{
	HL_USART_WORDLEN_8 = 0,
	HL_USART_WORDLEN_9 = 1
} EM_USART_Wordlen;

typedef enum
{
	HL_USART_STOPBITS_0_5 = 0,
	HL_USART_STOPBITS_1 = 1,
	HL_USART_STOPBITS_1_5 = 2,
	HL_USART_STOPBITS_2 = 3
} EM_USART_Stopbits;

typedef enum
{
	HL_USART_PARITY_EVEN = 0,
	HL_USART_PARITY_NO = 	1,
	HL_USART_PARITY_ODD = 	2
} EM_USART_Paity;

typedef enum
{
	HL_USART_HARDFLOW_CTS		 = 0,
	HL_USART_HARDFLOW_RTS 		 = 1,
	HL_USART_HARDFLOW_CTS_RTS	 = 2,
	HL_USART_HARDFLOW_NONE		 = 3
} EM_USART_Hardflow;

typedef enum
{
	HL_USART_MODE_RX 		= 0,
	HL_USART_MODE_TX 		= 1,
	HL_USART_MODE_RX_TX 	= 2
} EM_USART_Mode;

typedef enum
{
	HL_USART_SELECT_1 = 0,
	HL_USART_SELECT_2 = 1,
	HL_USART_SELECT_3 = 2
} EM_USART_Select;

typedef struct
{
	EM_USART_Select			USART_Interface;
	uint32_t 				USART_BaudRate;
	EM_USART_Hardflow		USART_HardwareFlowControl;
	EM_USART_Mode			USART_Mode;
	EM_USART_Paity			USART_Parity;
	EM_USART_Stopbits		USART_StopBits;
	EM_USART_Wordlen		USART_WordLength;
} USART_ConfigStructure;

void HL_USART_Configure(USART_ConfigStructure * USART_INIT_STRUCT);
void USART1_Send(uint8_t* data, uint8_t length);
void USART2_Send(uint8_t* data, uint8_t length);
void USART3_Send(uint8_t* data, uint8_t length);

#endif /* SRC_DRIVERS_LL_USART_USART_PUBLIC_H_ */
