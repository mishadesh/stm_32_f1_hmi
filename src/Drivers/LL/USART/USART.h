/*
 * USART_Init.h
 *
 *  Created on: 14 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef SYSTEM_INIT_INC_USART_H_
#define SYSTEM_INIT_INC_USART_H_

#include <GPIO.h>
#include <RCC.h>
#include <System_config.h>
#include "DMA.h"
#include "Modbus.h"
#include "stm32f10x_usart.h"
#include "string.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "stdbool.h"

#include "USART_Public.h"

extern QueueHandle_t SerialQueueIn;

#define RS485_RTS_IN()			GPIO_ResetBits ( GPIOA, GPIO_Pin_1 );
#define RS485_RTS_OUT()			GPIO_SetBits ( GPIOA, GPIO_Pin_1 );

#endif /* SYSTEM_INIT_INC_USART_H_ */
