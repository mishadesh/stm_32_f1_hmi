/*
 * USART_Init.c
 *
 *  Created on: 14 ��� 2019 �.
 *      Author: Mishadesh-x12
 */
#include <USART.h>

void USART1_IRQHandler(void);
void USART2_IRQHandler(void);
void USART3_IRQHandler(void);


uint8_t DMA_USART3_RX_BUF[DMA_USART3_RX_SIZE];
uint8_t DMA_USART3_TX_BUF[DMA_USART3_TX_SIZE];
uint8_t DMA_USART2_RX_BUF[DMA_USART2_RX_SIZE];
uint8_t DMA_USART2_TX_BUF[DMA_USART2_TX_SIZE];
uint8_t USART1_TX_BUF[DMA_USART1_TX_SIZE];
uint8_t USART1_LENGTH_TX = 0;

void HL_USART_Configure(USART_ConfigStructure * USART_INIT_STRUCT)
{
	if (USART_INIT_STRUCT != NULL)
	{
		USART_InitTypeDef USART_InitStruct;
		switch (USART_INIT_STRUCT->USART_HardwareFlowControl)
		{
		case HL_USART_HARDFLOW_CTS:
			USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_CTS;
			break;
		case HL_USART_HARDFLOW_CTS_RTS:
			USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_RTS_CTS;
			break;
		case HL_USART_HARDFLOW_RTS:
			USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_RTS;
			break;
		case HL_USART_HARDFLOW_NONE:
			USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
			break;
		default:
			USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
			break;
		}
		switch (USART_INIT_STRUCT->USART_Mode)
		{
		case HL_USART_MODE_RX:
			USART_InitStruct.USART_Mode = USART_Mode_Rx;
				break;
		case HL_USART_MODE_TX:
			USART_InitStruct.USART_Mode = USART_Mode_Tx;
			break;
		case HL_USART_MODE_RX_TX:
			USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
			break;
		default:
			USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
			break;
		}
		switch (USART_INIT_STRUCT->USART_Parity)
		{
		case HL_USART_PARITY_EVEN:
			USART_InitStruct.USART_Parity = USART_Parity_Even;
			break;
		case HL_USART_PARITY_ODD:
			USART_InitStruct.USART_Parity = USART_Parity_Odd;
			break;
		case HL_USART_PARITY_NO:
			USART_InitStruct.USART_Parity = USART_Parity_No;
			break;
		default:
			USART_InitStruct.USART_Parity = USART_Parity_No;
			break;
		}

		switch (USART_INIT_STRUCT->USART_StopBits)
		{
		case HL_USART_STOPBITS_0_5:
			USART_InitStruct.USART_StopBits = USART_StopBits_0_5;
			break;
		case HL_USART_STOPBITS_1:
			USART_InitStruct.USART_StopBits = USART_StopBits_1;
			break;
		case HL_USART_STOPBITS_1_5:
			USART_InitStruct.USART_StopBits = USART_StopBits_1_5;
			break;
		case HL_USART_STOPBITS_2:
			USART_InitStruct.USART_StopBits = USART_StopBits_2;
			break;
		default:
			USART_InitStruct.USART_StopBits = USART_StopBits_1;
			break;
		}

		switch (USART_INIT_STRUCT->USART_WordLength)
		{
		case HL_USART_WORDLEN_8:
			USART_InitStruct.USART_WordLength = USART_WordLength_8b;
			break;
		case HL_USART_WORDLEN_9:
			USART_InitStruct.USART_WordLength = USART_WordLength_9b;
			break;
		default:
			USART_InitStruct.USART_WordLength = USART_WordLength_8b;
			break;
		}

		USART_InitStruct.USART_BaudRate = USART_INIT_STRUCT->USART_BaudRate;

		switch(USART_INIT_STRUCT->USART_Interface)
		{
		case HL_USART_SELECT_1:
			USART_DeInit(USART1);
			GPIO_Init_USART_1();
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
			USART_Init(USART1, &USART_InitStruct);
			USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);
			USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
			USART_Cmd(USART1, ENABLE);
			NVIC_DMA_1_USART1_Init();
			break;
		case HL_USART_SELECT_2:
			//GPIO_Init_USART2_MANU_RTS();
						//RS485_RTS_IN();
			USART_DeInit(USART2);
			GPIO_Init_USART_2();

			RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
			USART_Init(USART2, &USART_InitStruct);
			USART_Cmd(USART2, ENABLE);
			USART_DMACmd(USART2, USART_DMAReq_Tx, ENABLE);
			USART_DMACmd(USART2, USART_DMAReq_Rx, ENABLE);
			USART_ITConfig(USART2, USART_IT_IDLE, ENABLE);
			USART_ITConfig(USART2, USART_IT_TC, ENABLE);
			DMA1_UART2_Init(DMA_USART2_RX_BUF, DMA_USART2_TX_BUF, DMA_USART2_RX_SIZE);
			GPIO_Init_USART2_MANU_RTS();
			RS485_RTS_IN();
			DMA_Cmd(DMA1_Channel6, ENABLE);

			break;
		case HL_USART_SELECT_3:
			USART_DeInit(USART3);
			GPIO_USART3_Init();
			RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
			USART_Init(USART3, &USART_InitStruct);
			DMA1_UART3_Init(DMA_USART3_RX_BUF, DMA_USART3_TX_BUF, DMA_USART3_RX_SIZE);
			USART_Cmd(USART3, ENABLE);
			USART_DMACmd(USART3, USART_DMAReq_Tx, ENABLE);
			USART_DMACmd(USART3, USART_DMAReq_Rx, ENABLE);
			USART_ITConfig(USART3, USART_IT_IDLE, ENABLE);
			DMA_Cmd(DMA1_Channel3, ENABLE);
			break;
		default:

			break;
		}
	}
	else
	{
		SET_ERROR_LED ;
	}
}

/*
void USART1_Init()
{
	GPIO_Init_USART_1();
	RCC_Init_USART1();

	USART_InitTypeDef usart1;
	usart1.USART_BaudRate = USART1_BD_9600;
	usart1.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	usart1.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	usart1.USART_Parity = USART_Parity_No;
	usart1.USART_StopBits = USART_StopBits_1;
	usart1.USART_WordLength = USART_WordLength_8b;

	USART_Init(USART1, &usart1);
	USART_Cmd(USART1, ENABLE);
	USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	NVIC_DMA_1_USART1_Init();

}

void USART2_Init()
{
	GPIO_Init_USART_2();
	RCC_Init_USART2();

	USART_InitTypeDef usart2;
	usart2.USART_BaudRate = USART1_BD_9600;
	usart2.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	usart2.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	usart2.USART_Parity = USART_Parity_No;
	usart2.USART_StopBits = USART_StopBits_1;
	usart2.USART_WordLength = USART_WordLength_8b;

	USART_Init(USART2, &usart2);
	USART_Cmd(USART2, ENABLE);
	USART_DMACmd(USART2, USART_DMAReq_Tx, ENABLE);
	USART_DMACmd(USART2, USART_DMAReq_Rx, ENABLE);
	USART_ITConfig(USART2, USART_IT_IDLE, ENABLE);
	USART_ITConfig(USART2, USART_IT_TC, ENABLE);

}

void USART2_DMA_Init(uint8_t* Rx_Buf_Addr, uint8_t* Tx_Buf_Addr, uint8_t RxTxbufSize)
{
	 USART2_Init();
	 DMA1_UART2_Init(Rx_Buf_Addr, Tx_Buf_Addr, RxTxbufSize);
	 GPIO_Init_USART2_MANU_RTS();
	 RS485_RTS_IN();
	 DMA_Cmd(DMA1_Channel6, ENABLE);
}

void USART1_DMA_Init(uint8_t* Rx_Buf_Addr, uint8_t* Tx_Buf_Addr, uint8_t RxTxbufSize)
{
	 USART1_Init();
	 DMA1_UART1_Init(Rx_Buf_Addr, Tx_Buf_Addr, RxTxbufSize);
	 DMA_Cmd(DMA1_Channel5, ENABLE);
}
*/
/*
void USART1_DMA_Send(uint8_t* data, uint8_t length)
{
	memcpy(&USART1_TX_BUF[0], data, length);
	DMA_Cmd(DMA1_Channel4, DISABLE);
	DMA_SetCurrDataCounter(DMA1_Channel4, length);
	DMA_Cmd(DMA1_Channel4, ENABLE);

}
*/

void USART1_Send(uint8_t* data, uint8_t length)
{
	memcpy(&USART1_TX_BUF[0], data, length);
	USART_SendData(USART1, USART1_TX_BUF[0]);
	USART1_LENGTH_TX = length;
	USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
}

void USART2_Send(uint8_t* data, uint8_t length)
{
	memcpy(&DMA_USART2_TX_BUF[0], data, length);
	DMA_Cmd(DMA1_Channel7, DISABLE);
	RS485_RTS_OUT ( ) ;
	DMA_SetCurrDataCounter(DMA1_Channel7, length);
	DMA_Cmd(DMA1_Channel7, ENABLE);

}

void USART3_Send(uint8_t* data, uint8_t length)
{
	memcpy(&DMA_USART3_TX_BUF[0], data, length);
	DMA_Cmd(DMA1_Channel2, DISABLE);
	DMA_SetCurrDataCounter(DMA1_Channel2, length);
	DMA_Cmd(DMA1_Channel2, ENABLE);

}

void USART1_IRQHandler(void)
{
	static uint8_t rx_cnt = 0, tx_cnt = 1;
	static Modbus_packet MbRecv = {0};
    if (USART_GetITStatus(USART1, USART_IT_IDLE) != RESET)
    {
    	BaseType_t xHigherPriorityTaskWoken;
    	USART1->DR;
    	USART_ClearITPendingBit(USART1, USART_IT_IDLE);
    	MbRecv.device = 0x01;
    	MbRecv.length = rx_cnt;
    	rx_cnt = 0;
    	xQueueSendFromISR(SerialQueueIn, &MbRecv, &xHigherPriorityTaskWoken);
    	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
    }
    if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {
    	USART_ClearITPendingBit(USART1, USART_IT_RXNE);
    	MbRecv.data[rx_cnt++] = USART_ReceiveData(USART1);
    }
    if (USART_GetITStatus(USART1, USART_IT_TXE) != RESET)
    {

    	if (tx_cnt >= USART1_LENGTH_TX)
    	{
    		USART1->DR;
    		USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
    		tx_cnt = 1; USART1_LENGTH_TX = 0;
    	}
    	else
    	{
    		USART_SendData(USART1, USART1_TX_BUF[tx_cnt++]);
    	}
    	 USART_ClearITPendingBit(USART1, USART_IT_TXE);
    }
}

void USART2_IRQHandler(void)
{

	if (USART_GetITStatus(USART2, USART_IT_TC) != RESET)
	{
		USART2->DR;
		USART_ClearITPendingBit(USART2, USART_IT_TC);
		RS485_RTS_IN();
	}


    if (USART_GetITStatus(USART2, USART_IT_IDLE) != RESET)
    {
    	USART2->DR;
    	USART_ClearITPendingBit(USART2, USART_IT_IDLE);
    	Modbus_packet MbRecv;
    	BaseType_t xHigherPriorityTaskWoken;

    	DMA_Cmd(DMA1_Channel6, DISABLE);

    	//put data here to queue
    	// user code
    	uint8_t datasize = DMA_USART2_RX_SIZE - DMA_GetCurrDataCounter(DMA1_Channel6);
    	memcpy(MbRecv.data, DMA_USART2_RX_BUF, datasize);
    	MbRecv.device = 0x02;
    	MbRecv.length = datasize;
    	xQueueSendFromISR(SerialQueueIn, &MbRecv, &xHigherPriorityTaskWoken);

    	//end user code
    	// DMA_USART1_RX_SIZE - DMA_GetCurrDataCounter(DMA1_Channel5);
    	//USART1_DMA_Send(DMA_USART1_RX_BUF, DMA_USART1_RX_SIZE - DMA_GetCurrDataCounter(DMA1_Channel5));
    	DMA_SetCurrDataCounter(DMA1_Channel6, DMA_USART2_RX_SIZE);
    	DMA_Cmd(DMA1_Channel6, ENABLE);

    	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);


    }
}

void USART3_IRQHandler(void)
{
    if (USART_GetITStatus(USART3, USART_IT_IDLE) != RESET)
    {
    	USART3->DR;
    	USART_ClearITPendingBit(USART3, USART_IT_IDLE);
    	Modbus_packet MbRecv;
    	BaseType_t xHigherPriorityTaskWoken;

    	DMA_Cmd(DMA1_Channel3, DISABLE);

    	//put data here to queue
    	// user code
    	uint8_t datasize = DMA_USART3_RX_SIZE - DMA_GetCurrDataCounter(DMA1_Channel3);
    	memcpy(MbRecv.data, DMA_USART3_RX_BUF, datasize);
    	MbRecv.device = 0x03;
    	MbRecv.length = datasize;
    	xQueueSendFromISR(SerialQueueIn, &MbRecv, &xHigherPriorityTaskWoken);

    	//end user code
    	// DMA_USART1_RX_SIZE - DMA_GetCurrDataCounter(DMA1_Channel5);
    	//USART1_DMA_Send(DMA_USART1_RX_BUF, DMA_USART1_RX_SIZE - DMA_GetCurrDataCounter(DMA1_Channel5));
    	DMA_SetCurrDataCounter(DMA1_Channel3, DMA_USART3_RX_SIZE);
    	DMA_Cmd(DMA1_Channel3, ENABLE);

    	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
    }
}
