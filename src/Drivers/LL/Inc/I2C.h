/*
 * I2C.h
 *
 *  Created on: 20 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef DRIVERS_LL_INC_I2C_H_
#define DRIVERS_LL_INC_I2C_H_

#include "GPIO.h"
#include "RCC.h"
#include "stm32f10x_i2c.h"


void I2C1_Init();
void I2C_StartTransmission(I2C_TypeDef* I2Cx, uint8_t transmissionDirection,  uint8_t slaveAddress);
void I2C_WriteData(I2C_TypeDef* I2Cx, uint8_t data);
uint8_t I2C_ReadData(I2C_TypeDef* I2Cx);
void I2C_StopTransmission(I2C_TypeDef* I2Cx);
uint8_t I2C_ReadByte8bitAddr(I2C_TypeDef* I2Cx, uint8_t slaveAddress, uint8_t reg_address);
void I2C_WriteByte8bitAddr(I2C_TypeDef* I2Cx, uint8_t slaveAddress, uint8_t reg_address, uint8_t data);
uint8_t I2C_ReadByte16bitAddr(I2C_TypeDef* I2Cx, uint8_t slaveAddress, uint16_t reg_address);
void I2C_WriteByte16bitAddr(I2C_TypeDef* I2Cx, uint8_t slaveAddress, uint16_t reg_address, uint8_t data);


#endif /* DRIVERS_LL_INC_I2C_H_ */
