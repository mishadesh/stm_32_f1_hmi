/*
 * FSMC_Init.h
 *
 *  Created on: 29 ���. 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef SYSTEM_INIT_INC_FSMC_H_
#define SYSTEM_INIT_INC_FSMC_H_

#include <GPIO.h>
#include <RCC.h>
#include "stm32f10x_fsmc.h"

void FSMC_Init();

#endif /* SYSTEM_INIT_INC_FSMC_H_ */
