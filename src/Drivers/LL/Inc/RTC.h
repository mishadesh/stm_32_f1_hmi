/*
 * RTC.h
 *
 *  Created on: 26 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef DRIVERS_LL_INC_RTC_H_
#define DRIVERS_LL_INC_RTC_H_

#include "stm32f10x_rcc.h"
#include "stm32f10x_rtc.h"
#include "stm32f10x_bkp.h"
#include "stm32f10x_pwr.h"

void RTC_Configuration(void);

#endif /* DRIVERS_LL_INC_RTC_H_ */
