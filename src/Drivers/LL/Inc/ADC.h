/*
 * ADC.h
 *
 *  Created on: 2 ���. 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef SRC_DRIVERS_LL_INC_ADC_H_
#define SRC_DRIVERS_LL_INC_ADC_H_

#include "stm32f10x_adc.h"
#include "RCC.h"
#include "GPIO.h"
#include "DMA.h"

void ADC1_Init();
uint16_t * ADC1_GetResultBufferAddress();


#endif /* SRC_DRIVERS_LL_INC_ADC_H_ */
