/*
 * GPIO_Init.h
 *
 *  Created on: 29 ���. 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef SYSTEM_INIT_INC_GPIO_H_
#define SYSTEM_INIT_INC_GPIO_H_
#include <RCC.h>
#include "stm32f10x_gpio.h"
//#include "stm32f10x_rcc.h"
void GPIO_Init_USART2_MANU_RTS();
void GPIO_Init_PD6_Led();
void GPIO_Init_PC6_Led();
void GPIO_Init_FSMC_Pinout();
void GPIO_Init_SPI1();
void GPIO_Init_SPI2();
void GPIO_Init_TouchPanel();
void GPIO_Init_USART_1();
void GPIO_Init_USART_2();
void GPIO_Init_I2C1_Pinout();
void GPIO_Init_I2C2_Pinout();
void GPIO_ADC1_PinIn();
void GPIO_USART3_Init();
#endif /* SYSTEM_INIT_INC_GPIO_H_ */
