/*
 * SPI_Init.h
 *
 *  Created on: 29 ���. 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef SYSTEM_INIT_INC_SPI_H_
#define SYSTEM_INIT_INC_SPI_H_

#include <GPIO.h>
#include <RCC.h>
#include "stm32f10x_spi.h"
#include "DMA.h"
#include "string.h"

void SPI_vInit_SPI2();
uint16_t SPI_Touch_Write ( uint8_t cmd, SPI_TypeDef* port );
void SPI2_DMA_Repeat();
void SPI2_DMA_Send(uint8_t* data, uint8_t len);
void SPI2_DMA_Enable();
void SPI2_DMA_Disable();
uint8_t * SPI2_GetRxBufAddr();
uint8_t * SPI2_GetTxBufAddr();

#endif /* SYSTEM_INIT_INC_SPI_H_ */
