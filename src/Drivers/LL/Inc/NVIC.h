/*
 * NVIC.h
 *
 *  Created on: 18 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef DRIVERS_LL_INC_NVIC_H_
#define DRIVERS_LL_INC_NVIC_H_

#include "misc.h"

void NVIC_DMA_1_USART1_Init();
void NVIC_DMA_1_USART2_Init();
void NVIC_DMA_1_USART3_Init();
void NVIC_DMA_1_SPI2_Init();
void NVIC_EXTI_TOUCH_Init();

#endif /* DRIVERS_LL_INC_NVIC_H_ */
