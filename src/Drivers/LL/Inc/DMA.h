/*
 * DMA.h
 *
 *  Created on: 18 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef DRIVERS_LL_INC_DMA_H_
#define DRIVERS_LL_INC_DMA_H_

#include <System_config.h>
#include "RCC.h"
#include "NVIC.h"
#include "stm32f10x_dma.h"
#include "stdbool.h"

//void DMA1_UART1_Init(uint8_t* Rx_Buf_Addr, uint8_t* Tx_Buf_Addr, uint8_t RxTxbufSize);
void DMA1_UART2_Init(uint8_t* Rx_Buf_Addr, uint8_t* Tx_Buf_Addr, uint8_t RxTxbufSize);
void DMA1_UART3_Init(uint8_t* Rx_Buf_Addr, uint8_t* Tx_Buf_Addr, uint8_t RxTxbufSize);
void DMA1_SPI2_Init(uint8_t* Rx_Buf_Addr, uint8_t* Tx_Buf_Addr, uint8_t RxTxbufSize);
void DMA1_ADC1_Init(uint16_t* ConvBuffAddr, uint8_t ConvBuffSize);

#endif /* DRIVERS_LL_INC_DMA_H_ */
