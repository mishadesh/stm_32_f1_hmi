/*
 * RCC_Init.h
 *
 *  Created on: 29 ���. 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef SYSTEM_INIT_INC_RCC_H_
#define SYSTEM_INIT_INC_RCC_H_
#include "stm32f10x_rcc.h"
typedef enum
{
	RCC_GPIOA = 0,
	RCC_GPIOB,
	RCC_GPIOC,
	RCC_GPIOD,
	RCC_GPIOE,
	RCC_GPIOF,
	RCC_GPIOG
}RCC_Pefiph;

void RCC_Init_Periph(RCC_Pefiph gpio);
void RCC_Init_GPIO_All();
void RCC_Init_FSMC();
void RCC_Init_SPI1();
void RCC_Init_SPI2();
void RCC_Init_USART1();
void RCC_Init_USART2();
void RCC_Init_DMA1();
void RCC_Init_I2C1();
void RCC_Init_AFIO();
void RCC_Init_ADC1();
void RCC_Init_I2C2();
#endif /* SYSTEM_INIT_INC_RCC_H_ */
