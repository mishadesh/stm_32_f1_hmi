/*
 * EXTI.h
 *
 *  Created on: 25 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef DRIVERS_LL_INC_EXTI_H_
#define DRIVERS_LL_INC_EXTI_H_

#include "stm32f10x_exti.h"
#include "RCC.h"
#include "stdbool.h"
#include "NVIC.h"

typedef void (*pvPB0ActionCb)();

void EXTI_TOUCH_IRQ_Init();
bool EXTI_bGetPB0_ActionEnable();
void EXTI_vSetPB0_ActionEnable(bool newflag);
void EXTI_vRegisterPB0ActionCb(pvPB0ActionCb newCb);



#endif /* DRIVERS_LL_INC_EXTI_H_ */
