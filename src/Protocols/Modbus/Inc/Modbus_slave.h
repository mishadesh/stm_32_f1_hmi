/*
 * Modbus_slave.h
 *
 *  Created on: 19 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef PROTOCOLS_INC_MODBUS_SLAVE_H_
#define PROTOCOLS_INC_MODBUS_SLAVE_H_

#include <System_config.h>
#include "stdint.h"
#include "Modbus.h"
#include "string.h"

#define SLAVE_REG_AMOUNT 16


Modbus_Error vProcessModbusSlave(Modbus_packet* request, Modbus_packet* response);
Modbus_Error vProcessModbus03(Modbus_packet* request, Modbus_packet* response);
Modbus_Error vProcessModbus06(Modbus_packet* request, Modbus_packet* response);
void vProcessModbusError(Modbus_Error eError, Modbus_packet* response, uint8_t cmd);


#endif /* PROTOCOLS_INC_MODBUS_SLAVE_H_ */
