/*
 * Crc16.h
 *
 *  Created on: 19 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef PROTOCOLS_INC_CRC16_H_
#define PROTOCOLS_INC_CRC16_H_

#include "stdint.h"

uint16_t Modbus_CRC16 (const uint8_t *nData, uint16_t uwLength);


#endif /* PROTOCOLS_INC_CRC16_H_ */
