/*
 * Modbus_slave.c
 *
 *  Created on: 19 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#include "Modbus_slave.h"
#include "Crc16.h"
#include "Modbus.h"

uint8_t Modbus_data[SLAVE_REG_AMOUNT *2] = {0};

Modbus_Error vProcessModbusSlave(Modbus_packet* request, Modbus_packet* response) //change type to be able to return errors
{
	Modbus_Error eResult = MODBUS_OK;
	if (request->data[DEVICE_ADDR] == MODBUS_SLAVE_ADDR)
	{
		response->device = request->device;
		if (((uint16_t)request->data[request->length-2]|(request->data[request->length-1]<<8))
			== Modbus_CRC16(request->data, request->length-2))
		{
			switch (request->data[CMD])
			{
				case  MODBUS_CMD_03:
					eResult = vProcessModbus03(request, response);
					break;
				case  MODBUS_CMD_06:
					eResult = vProcessModbus06(request, response);
					break;
				default:
					eResult = MODBUS_ERROR_UNSUPPORTED_FUNCTION;
					break;
			}
			if (eResult != MODBUS_OK)
			{
				vProcessModbusError(eResult, response, request->data[CMD]);
				eResult = MODBUS_ERROR_SEND;
			}
			uint16_t CRC_response;
			CRC_response = Modbus_CRC16(response->data, response->length);
			response->data[response->length] = (uint8_t)(CRC_response & 0x00FF);
			response->data[response->length+1] = (uint8_t)((CRC_response & 0xFF00)>>8);
			response->length +=2;
		}
		else
		{
			eResult = MODBUS_ERROR_CRC;
		}

	}
	return eResult;
}

Modbus_Error vProcessModbus03(Modbus_packet* request, Modbus_packet* response)
{
	Modbus_Error eResult = MODBUS_OK;
	uint16_t reg_addr, reg_amount;
	reg_addr = ((uint16_t) request->data[ADDR_LOW]| (request->data[ADDR_HIGH]<<8));
	reg_amount = ((uint16_t) request->data[DATA_LOW]| (request->data[DATA_HIGH]<<8));

	if (reg_addr < SLAVE_REG_AMOUNT)
	{
		if (reg_amount + reg_addr < SLAVE_REG_AMOUNT)
		{
			response->data[DEVICE_ADDR] = request->data[DEVICE_ADDR];
			response->data[CMD] = request->data[CMD];
			response->data[ADDR_HIGH] = reg_amount *2;
			memcpy(&response->data[3], Modbus_data, reg_amount *2);
			response->length = reg_amount *2 + 3; // + 1 + 1;
		}
		else
		{
			eResult = MODBUS_ERROR_WRONG_DATA;
		}
	}
	else
	{
		eResult = MODBUS_ERROR_WRONG_ADDR;
	}
	return eResult;

}

Modbus_Error vProcessModbus06(Modbus_packet* request, Modbus_packet* response)
{
	Modbus_Error eResult = MODBUS_OK;
	uint16_t reg_addr, value;
	reg_addr = ((uint16_t) request->data[ADDR_LOW]| (request->data[ADDR_HIGH]<<8));
	value = ((uint16_t) request->data[DATA_HIGH]| (request->data[DATA_LOW]<<8));

	if (reg_addr < SLAVE_REG_AMOUNT)
	{
		memcpy(&Modbus_data[reg_addr], &value, 2);
		memcpy(response->data, request->data, request->length-2);
		response->length =  request->length-2;
	}
	else
	{
		eResult = MODBUS_ERROR_WRONG_ADDR;
	}
	return eResult;
}

void vProcessModbusError(Modbus_Error eError, Modbus_packet* response, uint8_t cmd)
{
	switch (eError)
	{
		case MODBUS_ERROR_UNSUPPORTED_FUNCTION:
			response->data[ADDR_HIGH] = (uint8_t)EXCEPTION_ILLEGAL_FUNCTION;
			break;
		case MODBUS_ERROR_WRONG_ADDR:
			response->data[ADDR_HIGH] = (uint8_t)EXCEPTION_ILLEGAL_ADDRESS;
			break;
		case MODBUS_ERROR_WRONG_DATA:
			response->data[ADDR_HIGH] = (uint8_t)EXCEPTION_ILLEGAL_DATA;
			break;
		case MODBUS_ERROR_SLAVE_FAILURE:
			response->data[ADDR_HIGH] = (uint8_t)EXCEPTION_SLAVE_FAILURE;
			break;

		default:
			break;
	}
	response->data[DEVICE_ADDR] = (uint8_t)MODBUS_SLAVE_ADDR;
	response->data[CMD] = (uint8_t)(cmd | EXCEPTION_MASK);
	response->length = 3;
}
