/*
 * Utilites.h
 *
 *  Created on: 21 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef SYSTEM_INC_UTILITES_H_
#define SYSTEM_INC_UTILITES_H_

#include <System_config.h>

uint8_t bcd2bin(uint8_t bcd);
uint8_t bin2bcd(uint8_t bin);
uint8_t check_min_max(uint8_t val, uint8_t min, uint8_t max);
uint16_t GetCPUTemp(uint16_t ADC_Value);
#endif /* SYSTEM_INC_UTILITES_H_ */
