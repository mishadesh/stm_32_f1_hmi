/*
 * Delay.c
 *
 *  Created on: 29 ���. 2019 �.
 *      Author: Mishadesh-x12
 */


#include "Delay.h"

void vDelayMsCycle(uint32_t delay)
{
	delay*= SOFT_DELAY_MULTIPLIER_MS;
	while(delay!=0) delay --;
}

void vDelayUsCycle(uint32_t delay)
{
	delay*= SOFT_DELAY_MULTIPLIER_US;
	while(delay!=0) delay --;
}
/*
void vSysTickDelay(uint32_t delay)
{
	SysTickDelayMs = delay;
	while (SysTickDelayMs);
}
*/
