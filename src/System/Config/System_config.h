/*
 * System_config.h
 *
 *  Created on: 29 ���. 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef SYSTEM_CONFIG_SYSTEM_CONFIG_H_
#define SYSTEM_CONFIG_SYSTEM_CONFIG_H_

#include "stdint.h"
//#include "stm32f10x_gpio.h"
//#define NULL	0

#define SET_ERROR_LED		GPIO_SetBits ( GPIOC, GPIO_Pin_6 );
#define RESET_ERROR_LED		GPIO_ResetBits ( GPIOC, GPIO_Pin_6 );

#define F_CPU 128000000UL

#define MODBUS_PACKET_SIZE 100u

#define DMA_USART1_RX_SIZE 0u
#define DMA_USART1_TX_SIZE MODBUS_PACKET_SIZE

#define DMA_USART2_RX_SIZE MODBUS_PACKET_SIZE
#define DMA_USART2_TX_SIZE MODBUS_PACKET_SIZE

#define DMA_USART3_RX_SIZE MODBUS_PACKET_SIZE
#define DMA_USART3_TX_SIZE MODBUS_PACKET_SIZE

#define MODBUS_SLAVE_ADDR  0x01



#endif /* SYSTEM_CONFIG_SYSTEM_CONFIG_H_ */
