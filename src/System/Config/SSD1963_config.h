/*
 * SSD1963_config.h
 *
 *  Created on: 29 ���. 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef SYSTEM_CONFIG_SSD1963_CONFIG_H_
#define SYSTEM_CONFIG_SSD1963_CONFIG_H_

#define LCD_DATA    			    ((uint32_t)0x60020000)
#define LCD_REG   		  	   		((uint32_t)0x60000000)

#define RESET_PORT GPIOE
#define RESET_PIN  GPIO_Pin_1

#define DISP_WIDTH  800
#define DISP_HEIGHT 480

// Image orientation (can be 0, 90, 180, 270 degrees).
#define DISP_ORIENTATION    90

// Horizontal and vertical display resolution (from the glass datasheet).
#define DISP_HOR_RESOLUTION 800
#define DISP_VER_RESOLUTION 480

// Horizontal synchronization timing in pixels (from the glass datasheet).
#define DISP_HOR_PULSE_WIDTH        1
#define DISP_HOR_BACK_PORCH         46
#define DISP_HOR_FRONT_PORCH        210

// Vertical synchronization timing in lines (from the glass datasheet).
#define DISP_VER_PULSE_WIDTH        1
#define DISP_VER_BACK_PORCH         23
#define DISP_VER_FRONT_PORCH        22

#endif /* SYSTEM_CONFIG_SSD1963_CONFIG_H_ */
