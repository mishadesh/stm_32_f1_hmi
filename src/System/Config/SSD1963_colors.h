/*
 * colors.h
 *
 *  Created on: 17 окт. 2013 г.
 *      Author: vladimir
 */

#ifndef COLORS_H_
#define COLORS_H_


#define ColorBlack   0x0000
#define ColorNavy    0x0010
#define ColorBlue    0x001f
#define ColorGreen   0x0400
#define ColorTeal    0x0410
#define ColorLime    0x07e0
#define ColorAqua    0x07ff
#define ColorMaroon  0x8000
#define ColorPurple  0x8010
#define ColorOlive   0x8400
#define ColorGray    0x8410
#define ColorSilver  0xbdf7
#define ColorRed     0xf800
#define ColorYellow  0xffe0
#define ColorWhite   0xffff
#define ColorFuschia 0xf81f


#endif /* COLORS_H_ */
