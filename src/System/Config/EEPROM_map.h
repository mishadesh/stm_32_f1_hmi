/*
 * EEPROM_map.h
 *
 *  Created on: 23 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef SYSTEM_CONFIG_EEPROM_MAP_H_
#define SYSTEM_CONFIG_EEPROM_MAP_H_

#define TOUCH_COEF_AX		0x00
#define TOUCH_COEF_BX		0x02
#define TOUCH_COEF_AY		0x04
#define TOUCH_COEF_BY		0x06
#define TOUCH_IS_CALIBRATED	0x08

#endif /* SYSTEM_CONFIG_EEPROM_MAP_H_ */
