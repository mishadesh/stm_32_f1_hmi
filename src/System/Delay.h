/*
 * Delay.h
 *
 *  Created on: 29 ���. 2019 �.
 *      Author: Mishadesh-x12
 */

#ifndef SYSTEM_INC_DELAY_H_
#define SYSTEM_INC_DELAY_H_
#include <System_config.h>
#include "stm32f10x.h"

#define SOFT_DELAY_MULTIPLIER_MS		 (F_CPU)/1.28/10000
#define SOFT_DELAY_MULTIPLIER_US		 (F_CPU)/1.28/10000000


uint32_t SysTickDelayMs;

void vDelayMsCycle(uint32_t delay);
void vDelayUsCycle(uint32_t delay);
//void vSysTickDelay(uint32_t delay);

#endif /* SYSTEM_INC_DELAY_H_ */
