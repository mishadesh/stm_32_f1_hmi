/*
 * Utilites.c
 *
 *  Created on: 21 ��� 2019 �.
 *      Author: Mishadesh-x12
 */

#include "Utilites.h"

 const uint16_t V25 = 1750;// when V25=1.41V at ref 3.3V
 const uint16_t Avg_Slope = 5; //when avg_slope=4.3mV/C at ref 3.3V

/**
  ******************************************************************************
  *	@brief	Convert from BCD format to BIN format
  * @param	BCD value to be converted
  * @retval	BIN value from given BCD
  ******************************************************************************
  */
uint8_t bcd2bin(uint8_t bcd)
{
	uint8_t bin = (bcd >> 4) * 10;
	bin += bcd & 0x0F;

	return bin;
}

/**
  ******************************************************************************
  *	@brief	Convert from BIN format to BCD format
  * @param	BIN value to be converted
  * @retval	BCD value from given BIN
  ******************************************************************************
  */
uint8_t bin2bcd(uint8_t bin)
{
	uint8_t high = bin / 10;
	uint8_t low = bin - (high *10);

	return (high << 4) | low;
}

/**
  ******************************************************************************
  *	@brief	Check min and max from given value
  * @param	The value to be checked
  * @param	Allowed minimum value
  * @param	Allowed maximum value
  * @retval	Value between min and max or equal min or max
  ******************************************************************************
  */
uint8_t check_min_max(uint8_t val, uint8_t min, uint8_t max)
{
	if (val < min)
		return min;
	else if (val > max)
		return max;

	return val;
}

uint16_t GetCPUTemp(uint16_t ADC_Value)
{
	return (uint16_t)((V25-ADC_Value)/Avg_Slope+25);
}
