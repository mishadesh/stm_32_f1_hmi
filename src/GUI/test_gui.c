/*
 * test_gui.c
 *
 *  Created on: 3 ���. 2019 �.
 *      Author: Mishadesh-x12
 */


#include "test_gui.h"

static void _cbParent(WM_MESSAGE * pMsg) {
 switch (pMsg->MsgId) {
 case WM_PAINT:
 GUI_SetBkColor(GUI_BLUE);
 GUI_Clear();
 GUI_DispString("Parent window");
 break;
 }
}
static void _cbTool(WM_MESSAGE * pMsg) {
 switch (pMsg->MsgId) {
 case WM_PAINT:
 GUI_SetBkColor(GUI_RED);
 GUI_Clear();
 GUI_DispString("Tool window");
 break;
 }
}

void MainTask(void) {
 WM_HWIN hTool, hParent;
 WM_TOOLTIP_HANDLE hToolTip;

 GUI_Init();
 WM_SetDesktopColor(GUI_BLACK);
 hParent = WM_CreateWindow(0, 0, 200, 100, WM_CF_SHOW, _cbParent, 0);
 hTool = WM_CreateWindowAsChild(20, 20, 100, 50, hParent, WM_CF_SHOW, _cbTool, 0);
 hToolTip = WM_TOOLTIP_Create(hParent, NULL, 0);
 WM_TOOLTIP_AddTool(hToolTip, hTool, "I am a ToolTip");
}

